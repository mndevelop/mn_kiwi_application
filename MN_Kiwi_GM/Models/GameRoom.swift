//
//  GameRoom.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 2/24/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation
import UIKit

class GameRoom {
    
    var key = ""
    var name = "NoName"
    var tableViewCell: GameMasterRoomTableViewCell?
    var currentHelpStatus = "None"
    var isConnected : Bool = false
    var gameTimeLimit: TimeInterval = 0
    var gameOver = false
    var songName: String = ""
    var alertCode: UInt32 = 1021
    var themeImageName: String = ""
    
    var connectedRooms: Int = 0
    var connectedAccessories: Int = 0
    var cluesUsed: Int = 0
    
    init() {
        
    }
    
    init(name: String, gameTimeLimit: TimeInterval = 3600) {
        self.name = name
        self.gameTimeLimit = gameTimeLimit
    }
}
