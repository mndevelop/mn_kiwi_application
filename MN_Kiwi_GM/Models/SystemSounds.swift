//
//  SystemSounds.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 6/22/19.
//  Copyright © 2019 Mirabutaleb Nazari. All rights reserved.
//

import Foundation

var systemSoundIds : [UInt32] = [1016, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1052]

enum SYSTEMSOUNDS: UInt32 {
    case sound1 = 1016
    case sound2 = 1020
    case sound3 = 1021
    case sound4 = 1022
    case sound5 = 1023
    case sound6 = 1024
    case sound7 = 1025
    case sound8 = 1026
    case sound9 = 1027
    case sound10 = 1028
    case sound11 = 1029
    case sound12 = 1030
    case sound13 = 1031
    case sound14 = 1032
    case sound15 = 1033
    case sound16 = 1034
    case sound17 = 1035
    case sound18 = 1036
    case sound19 = 1052
}
