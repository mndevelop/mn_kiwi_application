//
//  Theme.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 5/5/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation
import UIKit

var themes = ["Default", "Theme 1", "Theme 2", "Theme 3", "Theme 4", "Theme 5", "Theme 6",  "Theme 7", "Theme 8", "Theme 9",  "Theme 10", "Theme 11", "Theme 12",  "Theme 13", "Theme 14", "Theme 15", "Theme 16", "Xmas 1", "Xmas 2", "Xmas 3", "Xmas 4", "Xmas 5"]
var themeImageNames = ["Default", "timer1", "timer2", "timer3", "timer4", "timer5", "timer6", "timer7", "timer8", "timer9", "timer10", "timer11", "timer12", "timer13", "timer14", "timer15", "timer16", "timer17", "timer18", "timer19", "timer20", "timer21"]

struct Theme {
    
    var title: String = ""
    var imageName: String = ""
    
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
}

