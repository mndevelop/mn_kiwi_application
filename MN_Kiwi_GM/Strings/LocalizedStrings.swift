//
//  LocalizedStrings.swift
//  MN_Kiwi_GM
//
//  Created by Joshua Shroyer on 10/20/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation

enum LocalizedStrings {
    
    static let okButton = NSLocalizedString("OK", comment: "Title of button used to accept.")
    static let cancelButton = NSLocalizedString("Cancel", comment: "Title of button used to cancel or quit operation.")
    static let errorTitle = NSLocalizedString("Error", comment: "Alert title indicating a problem occured.")
    
    static let networkErrorAlertTitle = NSLocalizedString("Network Error", comment: "Alert title indicating the operation failed because there was no connection to the internet.")
    static let networkErrorAlertMessage = NSLocalizedString("Log in could not complete because there is no network connection. Please check your connection and try again later.", comment: "Alert message indicating the operation failed because there was no connection to the internet.")
    
    enum Login {
        static let emptyFieldAlertTitle = NSLocalizedString("Missing Information", comment: "Alert title indicating that one of the required fields is not filled out.")
        static let emptyFieldAlertMessage = NSLocalizedString("One or more of the login fields is empty. Please make sure to enter your email and password to sign up or login.", comment: "Alert message describing to the user that one of the required fields is not filled out.")
        
        static let forgotPasswordEmptyFieldAlertMessage = NSLocalizedString("You must enter an email to reset password.", comment: "Alert message stating that the required email field is not filled out.")
        
        static let sendForgotPasswordButton = NSLocalizedString("Send Reset Password Email", comment: "Button title stating the user will receive an email once pressed.")
        
        static let forgotPasswordAlertTitle = NSLocalizedString("Email Request Sent", comment: "Alert title indicating that the requested email has been sent.")
        static let forgotPasswordAlertMessage = NSLocalizedString("Please check your email for further instructions.", comment: "Alert message asking the user to check the email they provided.")
        
        static let loginButton = NSLocalizedString("Log In", comment: "Button title to sign in to the application using an existing account.")
        static let signupButton = NSLocalizedString("Sign Up", comment: "Button title to sign up for the application by creating a new account.")
        
        static let loginFailedAlertTitle = NSLocalizedString("Log In Failed", comment: "Alert title indicating logging in was unsuccessful.")
        static let invalidCredentialsAlertMessage = NSLocalizedString("No account found with provided email and password. Please check again or sign up for a new account.", comment: "Alert message indicating the credentials provided were invalid.")
        
        static func loginFailed(with error: Error) -> String {
            let loginFailedWithErrorFormat = NSLocalizedString("An error occurred while logging in: %1$@", comment: "Alert message indicating a general log in failure displaying the error's description.")
            return String.localizedStringWithFormat(loginFailedWithErrorFormat, error.localizedDescription)
        }
        
        static let signUpFailedAlertTitle = NSLocalizedString("Sign Up Failed", comment: "Alert title indicating signing up for a new account was unsuccessful.")
        static func signUpFailed(with error: Error) -> String {
            let signupFailedWithErrorFormat = NSLocalizedString("An error occurred while signing up: %1$@", comment: "Alert message indicating a general sign up failure displaying the error's description.")
            return String.localizedStringWithFormat(signupFailedWithErrorFormat, error.localizedDescription)
        }
        
        static let forgotPasswordFailedAlertTitle = NSLocalizedString("Forgot Password Failed", comment: "Alert title indicating the forgot password operation was unsuccessful.")
        static func forgotPasswordFailed(with error: Error) -> String {
            let forgotPasswordFailedWithErrorFormat = NSLocalizedString("An error occurred while attempting to send the forgot password request: %1$@", comment: "Alert message indicating a general forgot password operation failure displaying the error's description.")
            return String.localizedStringWithFormat(forgotPasswordFailedWithErrorFormat, error.localizedDescription)
        }
        
        static let invalidEmailAlertMessage = NSLocalizedString("Email is invalid. Please check to make sure your email is correct.", comment: "Alert message indicating the email provided is malformed.")
        static let emailAlreadyInUseAlertMessage = NSLocalizedString("Email is already registered to an existing account.", comment: "Alert message indicating the email provided is already used on a registered account.")
        
    }
    
}
