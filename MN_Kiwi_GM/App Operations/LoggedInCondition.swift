//
//  LoggedInCondition.swift
//  MN_Kiwi_GM
//
//  Created by Joshua Shroyer on 10/18/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation
import Firebase

struct LoggedInCondition: OperationCondition {
    static let name = "LoggedIn"
    static let isMutuallyExclusive = false
    
    func dependencyForOperation(operation: Operation) -> Operation? {
        return nil // Nothing we can do to fix this - User is logged in or not
    }
    
    func evaluateForOperation(operation: Operation, completion: @escaping (OperationConditionResult) -> Void) {
        guard let _ = Auth.auth().currentUser else {
            let error = NSError(code: .conditionFailed, userInfo: [OperationConditionKey: LoggedInCondition.name])
            completion(.failed(error))
            return
        }
        
        completion(.satisfied)
    }
    
    
}
