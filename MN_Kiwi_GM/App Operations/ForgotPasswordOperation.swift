//
//  ForgotPasswordOperation.swift
//  MN_Kiwi_GM
//
//  Created by Joshua Shroyer on 10/21/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation
import Firebase

final class ForgotPasswordOperation: MNOperation {
    let email: String
    
    init(email: String) {
        self.email = email
        
        super.init()
        
        addCondition(condition: RequiredFieldsAddressedCondition(requiredFields: email))
        addCondition(condition: ReachabilityCondition(host: URL(string: "https://mn-kiwi-gm.firebaseio.com")!))
    }
    
    override func execute() {
        Auth.auth().sendPasswordReset(withEmail: email, completion: { [weak self] (error) in
            
            guard let strongSelf = self else { return }
            
            guard error == nil else {
                strongSelf.finishWithError(error: error)
                return
            }
            
            strongSelf.finish()
        })
    }
    
    override func finished(errors: [Error]) {
        
        let alert = AlertOperation()
        
        // Alert is shown whether there's an error or on success
        // Produce the alert operation when the function is done.
        defer {
            alert.addAction(title: LocalizedStrings.okButton, style: .default)
            produceOperation(operation: alert)
        }
        
        guard let error = errors.first as NSError? else {
            alert.title = LocalizedStrings.Login.forgotPasswordAlertTitle
            alert.message = LocalizedStrings.Login.forgotPasswordAlertMessage
            return
        }
        
        let errorReason = (
            error.domain,
            error.code,
            error.userInfo[OperationConditionKey] as? String
        )
        
        let failedReachability = (
            OperationErrorDomain,
            OperationErrorCode.conditionFailed,
            ReachabilityCondition.name
        )
        
        let invalidRecipientEmail = (
            AuthErrorDomain,
            AuthErrorCode.invalidRecipientEmail,
            nil as String?
        )
        
        let emptyEmail = (
            OperationErrorDomain,
            OperationErrorCode.conditionFailed,
            RequiredFieldsAddressedCondition.name
        )
        
        
        switch errorReason {
        case emptyEmail:
            alert.title = LocalizedStrings.Login.emptyFieldAlertTitle
            alert.message = LocalizedStrings.Login.forgotPasswordEmptyFieldAlertMessage
        case failedReachability:
            alert.title = LocalizedStrings.networkErrorAlertTitle
            alert.message = LocalizedStrings.networkErrorAlertMessage
        case invalidRecipientEmail:
            alert.title = LocalizedStrings.Login.signUpFailedAlertTitle
            alert.message = LocalizedStrings.Login.invalidEmailAlertMessage
        default:
            alert.title = LocalizedStrings.Login.forgotPasswordFailedAlertTitle
            alert.message = LocalizedStrings.Login.forgotPasswordFailed(with: error)
        }
        
    }
}

// Operators to use in the switch statement.
private func ~=(lhs: (String, OperationErrorCode, String?),
                rhs: (String, Int, String?)) -> Bool {
    return lhs.0 ~= rhs.0 && lhs.1.rawValue ~= rhs.1 && lhs.2 == rhs.2
}

private func ~=(lhs: (String, OperationErrorCode, String),
                rhs: (String, Int, String?)) -> Bool {
    return lhs.0 ~= rhs.0 && lhs.1.rawValue ~= rhs.1 && lhs.2 == rhs.2
}

private func ~=(lhs: (String, AuthErrorCode, String?),
                rhs: (String, Int, String?)) -> Bool {
    return lhs.0 ~= rhs.0 && lhs.1.rawValue ~= rhs.1 && lhs.2 == rhs.2
}
