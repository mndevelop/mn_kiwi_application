//
//  SignUpOperation.swift
//  MN_Kiwi_GM
//
//  Created by Joshua Shroyer on 10/20/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation
import Firebase

final class SignUpOperation: MNOperation {
    
    static let signUpErrorTypeKey = "SignUpErrorTypeKey"
    
    let email: String
    let password: String
    let ref = Database.database().reference()
    
    enum SignUpErrorType {
        case signUpFailed
    }
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
        
        super.init()
        
        userInitiated = true
        
        addCondition(condition: AuthenticationContext())
        addCondition(condition: RequiredFieldsAddressedCondition(requiredFields: email, password))
        addCondition(condition: ReachabilityCondition(host: URL(string: "https://mn-kiwi-gm.firebaseio.com")!))
        
        addObserver(observer: NetworkObserver())
    }
    
    override func execute() {
        
        Auth.auth().createUser(withEmail: email, password: password, completion: { [weak self] (authResult, error) in
            
            guard let strongSelf = self else { return }
            
            guard error == nil else {
                strongSelf.finishWithError(error: error)
                return
            }
            
            guard let user = authResult?.user else {
                let error = NSError(code: .executionFailed, userInfo: [SignUpOperation.signUpErrorTypeKey: SignUpErrorType.signUpFailed])
                strongSelf.finishWithError(error: error)
                return
            }
            
            // Set the default `Game Limit` value to 1
            let userID = user.uid
            strongSelf.ref.child(userID).child("Game Limit").setValue(1)
            
            strongSelf.finish()
        })
    }
    
    override func finished(errors: [Error]) {
        guard let error = errors.first as NSError? else { return }
        
        let errorReason = (
            error.domain,
            error.code,
            error.userInfo[OperationConditionKey] as? String,
            error.userInfo[SignUpOperation.signUpErrorTypeKey] as? SignUpErrorType
        )
        
        let failedReachability = (
            OperationErrorDomain,
            OperationErrorCode.conditionFailed,
            ReachabilityCondition.name,
            nil as SignUpErrorType?
        )
        
        let emptyEmailOrPassword = (
            OperationErrorDomain,
            OperationErrorCode.conditionFailed,
            RequiredFieldsAddressedCondition.name,
            nil as SignUpErrorType?
        )
        
        let invalidEmail = (
            AuthErrorDomain,
            AuthErrorCode.invalidEmail,
            nil as String?,
            nil as SignUpErrorType?
        )
        
        let emailAlreadyInUse = (
            AuthErrorDomain,
            AuthErrorCode.emailAlreadyInUse,
            nil as String?,
            nil as SignUpErrorType?
        )
        
        let weakPassword = (
            AuthErrorDomain,
            AuthErrorCode.weakPassword,
            nil as String?,
            nil as SignUpErrorType?
        )
        
        let alert = AlertOperation()
        
        switch errorReason {
        case emptyEmailOrPassword:
            alert.title = LocalizedStrings.Login.emptyFieldAlertTitle
            alert.message = LocalizedStrings.Login.emptyFieldAlertMessage
        case failedReachability:
            alert.title = LocalizedStrings.networkErrorAlertTitle
            alert.message = LocalizedStrings.networkErrorAlertMessage
        case invalidEmail:
            alert.title = LocalizedStrings.Login.signUpFailedAlertTitle
            alert.message = LocalizedStrings.Login.invalidEmailAlertMessage
        case emailAlreadyInUse:
            alert.title = LocalizedStrings.Login.signUpFailedAlertTitle
            alert.message = LocalizedStrings.Login.emailAlreadyInUseAlertMessage
        case weakPassword:
            alert.title = LocalizedStrings.Login.signUpFailedAlertTitle
            guard let weakPasswordDescription = error.userInfo[NSLocalizedFailureReasonErrorKey] as? String else {
                fatalError("Error missing failure reason for sign up.")
            }
            alert.message = weakPasswordDescription
        default:
            alert.title = LocalizedStrings.Login.signUpFailedAlertTitle
            alert.message = LocalizedStrings.Login.signUpFailed(with: error)
        }
        
        
        // No custom action for this button.
        alert.addAction(title: LocalizedStrings.okButton, style: .default)
        produceOperation(operation: alert)
    }

}


// Operators to use in the switch statement.
private func ~=(lhs: (String, OperationErrorCode, String?, SignUpOperation.SignUpErrorType),
                rhs: (String, Int, String?, SignUpOperation.SignUpErrorType?)) -> Bool {
    return lhs.0 ~= rhs.0 && lhs.1.rawValue ~= rhs.1 && lhs.2 == rhs.2 && lhs.3 ~= rhs.3
}

private func ~=(lhs: (String, OperationErrorCode, String, SignUpOperation.SignUpErrorType?),
                rhs: (String, Int, String?, SignUpOperation.SignUpErrorType?)) -> Bool {
    return lhs.0 ~= rhs.0 && lhs.1.rawValue ~= rhs.1 && lhs.2 == rhs.2 && lhs.3 ~= rhs.3
}

private func ~=(lhs: (String, AuthErrorCode, String?, SignUpOperation.SignUpErrorType?),
                rhs: (String, Int, String?, SignUpOperation.SignUpErrorType?)) -> Bool {
    return lhs.0 ~= rhs.0 && lhs.1.rawValue ~= rhs.1 && lhs.2 == rhs.2 && lhs.3 ~= rhs.3
}
