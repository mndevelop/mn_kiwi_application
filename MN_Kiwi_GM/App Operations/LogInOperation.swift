//
//  LogInOperation.swift
//  MN_Kiwi_GM
//
//  Created by Joshua Shroyer on 10/14/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation
import Firebase

enum Authentication {}
typealias AuthenticationContext = MutuallyExclusive<Authentication>

class LogInOperation: MNOperation {
    static let loginErrorType = "LoginErrorType"
    
    // MARK: Properties
    let email: String
    let password: String
    
    fileprivate enum LogInErrorType {
        case incorrectCredentials
    }
    
    // MARK: Initialization
    init(email: String, password: String) {
        self.email = email
        self.password = password
        
        super.init()
        
        self.userInitiated = true
        
        addCondition(condition: AuthenticationContext())
        addCondition(condition: RequiredFieldsAddressedCondition(requiredFields: email, password))
        addCondition(condition: ReachabilityCondition(host: URL(string: "https://mn-kiwi-gm.firebaseio.com")!))
        
        addObserver(observer: NetworkObserver())
    }
    
    override func execute() {
        // Sign in with the provided email and password
        Auth.auth().signIn(withEmail: email, password: password, completion: { [weak self] (user, error) in
            
            var error: Error?
            
            if let strongError = error {
                error = strongError
            }
            
            if user == nil {
                error = NSError(code: .executionFailed, userInfo: [LogInOperation.loginErrorType: LogInErrorType.incorrectCredentials])
            }
            
            self?.finishWithError(error: error)
            
        })
    }
    
    override func finished(errors: [Error]) {
        guard let error = errors.first as NSError? else { return }
        
        let errorReason = (
            error.domain,
            error.code,
            error.userInfo[OperationConditionKey] as? String,
            error.userInfo[LogInOperation.loginErrorType] as? LogInErrorType
        )

        let failedReachability = (
            OperationErrorDomain,
            OperationErrorCode.conditionFailed,
            ReachabilityCondition.name,
            nil as LogInErrorType?
        )

        let emptyEmailOrPassword = (
            OperationErrorDomain,
            OperationErrorCode.conditionFailed,
            RequiredFieldsAddressedCondition.name,
            nil as LogInErrorType?
        )

        let incorrectCredentials = (
            OperationErrorDomain,
            OperationErrorCode.executionFailed,
            nil as String?,
            LogInErrorType.incorrectCredentials
        )
        
        let alert = AlertOperation()
        
        switch errorReason {
        case emptyEmailOrPassword:
            alert.title = LocalizedStrings.Login.emptyFieldAlertTitle
            alert.message = LocalizedStrings.Login.emptyFieldAlertMessage
        case failedReachability:
            alert.title = LocalizedStrings.networkErrorAlertTitle
            alert.message = LocalizedStrings.networkErrorAlertMessage
        case incorrectCredentials:
            alert.title = LocalizedStrings.Login.loginFailedAlertTitle
            alert.message = LocalizedStrings.Login.invalidCredentialsAlertMessage
        default:
            alert.title = LocalizedStrings.Login.loginFailedAlertTitle
            alert.message = LocalizedStrings.Login.loginFailed(with: error)
        }
        
        
        // No custom action for this button.
        alert.addAction(title: LocalizedStrings.okButton, style: .default)
        produceOperation(operation: alert)
    }
}

// Operators to use in the switch statement.
private func ~=(lhs: (String, OperationErrorCode, String?, LogInOperation.LogInErrorType),
                rhs: (String, Int, String?, LogInOperation.LogInErrorType?)) -> Bool {
    return lhs.0 ~= rhs.0 && lhs.1.rawValue ~= rhs.1 && lhs.2 == rhs.2 && lhs.3 ~= rhs.3
}

private func ~=(lhs: (String, OperationErrorCode, String, LogInOperation.LogInErrorType?),
                rhs: (String, Int, String?, LogInOperation.LogInErrorType?)) -> Bool {
    return lhs.0 ~= rhs.0 && lhs.1.rawValue ~= rhs.1 && lhs.2 == rhs.2 && lhs.3 ~= rhs.3
}
