//
//  RequiredFieldsAddressedCondition.swift
//  MN_Kiwi_GM
//
//  Created by Joshua Shroyer on 10/20/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation

struct RequiredFieldsAddressedCondition: OperationCondition {
    static let name = "RequiredFieldsAddressedCondition"
    static let isMutuallyExclusive = false
    
    let requiredFields: [String]
    
    init(requiredFields: [String]) {
        self.requiredFields = requiredFields
    }
    
    init(requiredFields: String...) {
        self.requiredFields = requiredFields
    }
    
    func dependencyForOperation(operation: Operation) -> Operation? { return nil }
    
    func evaluateForOperation(operation: Operation, completion: @escaping (OperationConditionResult) -> Void) {
        guard self.requiredFields.allSatisfy({$0.hasText}) else {
            let error = NSError(code: .conditionFailed, userInfo: [OperationConditionKey: RequiredFieldsAddressedCondition.name])
            completion(.failed(error))
            return
        }
        
        completion(.satisfied)
    }
    
    
}
