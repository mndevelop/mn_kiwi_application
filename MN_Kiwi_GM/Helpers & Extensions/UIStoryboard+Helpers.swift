//
//  UIStoryboard+Helpers.swift
//  MN_Kiwi_GM
//
//  Created by Joshua Shroyer on 10/20/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit

extension UIStoryboard {
    static let main = UIStoryboard.init(name: "Main", bundle: nil)
}
