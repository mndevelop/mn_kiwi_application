//
//  UIViewExtensions.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 3/4/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func applyDropShadow(shadowColor: UIColor = .black,
                         masksToBounds: Bool = false,
                         opacity: Float = 0.5,
                         shadowOffsetHeight: Double = 2.0,
                         shadowOffsetWidth: Double = 0.0,
                         radius: CGFloat = 6.0) {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = masksToBounds
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
        self.layer.shadowOpacity = opacity
        self.layer.cornerRadius = radius
        self.layer.shadowPath = shadowPath.cgPath
    }
}

extension UIColor {
    
    static func kiwiGrayTransparent() -> UIColor {
        return UIColor.init(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
    }
    
    static func kiwiWhiteTransparent() -> UIColor {
        return UIColor.init(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0)
    }
    
    static func kiwiGreen() -> UIColor {
        return UIColor.init(red: 193/255, green: 220/255, blue: 74/255, alpha: 1.0)
    }
    
    static func kiwiDarkerGreen() -> UIColor {
        return UIColor.init(red: 152/255, green: 198/255, blue: 38/255, alpha: 1.0)
    }
    
    static func kiwiYellow() -> UIColor {
        return UIColor.init(red: 236/255, green: 195/255, blue: 11/255, alpha: 1.0)
    }
    
    static func kiwiRed() -> UIColor {
        return UIColor.init(red: 255/255, green: 86/255, blue: 83/255, alpha: 1.0)
    }
    
    static func kiwiScarletRed() -> UIColor {
        return UIColor.init(red: 204/255, green: 0/255, blue: 0/255, alpha: 1.0)
    }
    
    static func kiwiBlue() -> UIColor {
        return UIColor.init(red: 33/255, green: 216/255, blue: 236/255, alpha: 1.0)
    }
    
    static func kiwiPeterRiverBlue() -> UIColor {
        return UIColor.init(red: 52/255, green: 152/255, blue: 219/255, alpha: 1.0)
    }
    
    static func kiwiRoyalBlue() -> UIColor {
        return UIColor.init(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0)
    }
    
    static func kiwiNavyBlue() -> UIColor {
        return UIColor.init(red: 44/255, green: 62/255, blue: 80/255, alpha: 1.0)
    }

    static func kiwiDarkBlueGray() -> UIColor {
        return UIColor.init(red: 97/255, green: 103/255, blue: 115/255, alpha: 1.0)
    }
    
    static func kiwiLightBlueGray() -> UIColor {
        return UIColor.init(red: 128/255, green: 136/255, blue: 153/255, alpha: 1.0)
    }
    
    static func kiwiGold() -> UIColor {
        return UIColor.init(red: 255/255, green: 181/255, blue: 0/255, alpha: 1.0)
    }
    
    static func kiwiMandarin() -> UIColor {
        return UIColor.init(red: 243/255, green: 119/255, blue: 72/255, alpha: 1.0)
    }
}
