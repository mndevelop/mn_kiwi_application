//
//  FirebaseUserManager.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 2/24/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation
import Firebase

struct FirebaseUserManager {
    func getCurrentUser() -> User? {
        return Auth.auth().currentUser
    }
}
