//
//  AppSettingsViewController.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 5/8/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import Firebase

class AppSettingsViewController: UIViewController {

    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var changeEmail: UIButton!
    @IBOutlet weak var setGameMasterPin: UIButton!
    @IBOutlet weak var setAlertSoundButton: UIButton!
    @IBOutlet weak var resetGameCacheButton: UIButton!
    
    var gameList : [GameRoom] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Settings"
        
        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func setupUI() {
        self.view.layoutIfNeeded()
        changePasswordButton.applyDropShadow()
        changeEmail.applyDropShadow()
        setGameMasterPin.applyDropShadow()
//        setAlertSoundButton.applyDropShadow()
        resetGameCacheButton.applyDropShadow()
    }
    
    @IBAction func changePasswordButtonAction(_ sender: UIButton) {
        // Change Password UI
    }
    
    @IBAction func setGameMasterPinAction(_ sender: UIButton) {
        // Open set pin UI
        let storyBoard = UIStoryboard(name: "LockScreen", bundle: nil)
        if let lockViewController = storyBoard.instantiateInitialViewController() as? LockViewController {
            lockViewController.senderVC = self
            self.present(lockViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func lockAdminAbilitiesAction(_ sender: UIButton) {
        // Open lock admin abilities UI
    }
    
    @IBAction func setAlertSoundsAction(_ sender: UIButton) {
        // Open set alert sound UI
    }
    
    @IBAction func resetCacheAction(_ sender: UIButton) {
        // Reset/Remove Current Games Firebase Reference
        // Clear all outstanding observers? Need access to game list or something for this.
        guard let userId = FirebaseUserManager().getCurrentUser()?.uid else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        let alert = UIAlertController(title: "Reset Game Status Cache", message: "This action will clear game status information (Timer, Chat, etc.) for ALL ROOMS. If you have any running games, they will need to be closed on the device and reopened to work properly.\n\nPlease Note: You WILL NOT LOSE any stored clues or hints on the games. Are you sure you want to do this?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Confirm", style: .destructive, handler: { (alert) in
            
            for game in self.gameList {
                Database.database().reference().child(userId).child("Current Games").child(game.name).child("helpStatus").setValue("Disconnected")
            }
            
            Database.database().reference().child(userId).child("Current Games").removeValue()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
