//
//  ChangePasswordViewController.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 5/10/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import FirebaseAuth

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var formBackgroundView: UIView!
    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var setNewPasswordButton: UIButton!
    
    var selectedField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        styleUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            let offset: CGFloat = 120
            
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height - offset
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
    }
    
    func styleUI() {
        self.view.layoutIfNeeded()
        formBackgroundView.applyDropShadow(radius: 6)
        currentPasswordTextField.applyDropShadow(radius: 6)
        newPasswordTextField.applyDropShadow(radius: 6)
        confirmPasswordTextField.applyDropShadow(radius: 6)
        setNewPasswordButton.applyDropShadow(radius: 6)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func showPasswordSuccess() {
        let alert = UIAlertController(title: "Password Reset", message: "Your password has been successfully reset!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPasswordError() {
        let alert = UIAlertController(title: "Error Setting Password", message: "Please check your credentials and try again.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showUserRetrieveError() {
        let alert = UIAlertController(title: "Error Getting User Information", message: "Please logout of the application and try again.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func reAuthUser(currentPass: String, newPass: String) {
        guard let user = FirebaseUserManager().getCurrentUser() else {
            showUserRetrieveError()
            return
        }
        
        guard let email = user.email else {
            showUserRetrieveError()
            return
        }
        
        let credential = EmailAuthProvider.credential(withEmail: email, password: currentPass)
        user.reauthenticateAndRetrieveData(with: credential, completion: { (result, error) in
            
            if error != nil {
                self.showPasswordError()
                return
            }
            
            user.updatePassword(to: newPass, completion: { (error) in
                if error != nil {
                    self.showPasswordError()
                    return
                }
                
                self.showPasswordSuccess()
                
            })
        })
    }
    
    func setPassword() {
        guard let currentPass = currentPasswordTextField.text else {
            showPasswordError()
            return
        }
        
        guard let newPass = newPasswordTextField.text else {
            showPasswordError()
            return
        }
        
        guard let confirmPass = confirmPasswordTextField.text else {
            showPasswordError()
            return
        }
        
        if currentPass.isEmpty || newPass.isEmpty || confirmPass.isEmpty {
            showPasswordError()
            return
        }
        
        if newPass != confirmPass {
            showPasswordError()
            return
        }
        
        reAuthUser(currentPass: currentPass, newPass: newPass)
        
        currentPasswordTextField.text = ""
        newPasswordTextField.text = ""
        confirmPasswordTextField.text = ""
    }
    
    @IBAction func setNewPasswordAction(_ sender: UIButton) {
        setPassword()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedField = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == currentPasswordTextField {
            newPasswordTextField.becomeFirstResponder()
        } else if textField == newPasswordTextField {
            confirmPasswordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            setPassword()
        }
        
        return true
    }
}
