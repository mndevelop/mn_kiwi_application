//
//  ChangeEmailViewController.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 5/11/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import FirebaseAuth

class ChangeEmailViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var formBackgroundView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var changeEmailButton: UIButton!
    
    var selectedField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        styleUI()
        NotificationCenter.default.addObserver(self, selector: #selector(ChangeEmailViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChangeEmailViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func styleUI() {
        self.view.layoutIfNeeded()
        formBackgroundView.applyDropShadow(radius: 6)
        passwordTextField.applyDropShadow(radius: 6)
        emailTextField.applyDropShadow(radius: 6)
        changeEmailButton.applyDropShadow(radius: 6)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            let offset: CGFloat = 120
            
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height - offset
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
    }
    
    func showUserRetrieveError() {
        let alert = UIAlertController(title: "Error Getting User Information", message: "Please logout of the application and try again.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showEmailSuccess() {
        let alert = UIAlertController(title: "Email Reset", message: "Your email has been successfully reset!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showEmailError() {
        let alert = UIAlertController(title: "Error Changing Email", message: "Please check your credentials and try again.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func reAuthUserAndChangeEmail(currentPass: String, email: String) {
        guard let user = FirebaseUserManager().getCurrentUser() else {
            showUserRetrieveError()
            return
        }
        
        guard let userEmail = user.email else {
            showUserRetrieveError()
            return
        }
        
        let credential = EmailAuthProvider.credential(withEmail: userEmail, password: currentPass)
        
        user.reauthenticateAndRetrieveData(with: credential, completion: { (result, error) in
            if error != nil {
                self.showEmailError()
                return
            }
            
            user.updateEmail(to: email, completion: { (error) in
                
                if error != nil {
                    self.showEmailError()
                    return
                }
                
                self.showEmailSuccess()
            })
        })
    }
    
    func changeEmail() {
        guard let currentPass = passwordTextField.text else {
            showEmailError()
            return
        }
        
        guard let email = emailTextField.text else {
            showEmailError()
            return
        }
        
        reAuthUserAndChangeEmail(currentPass: currentPass, email: email)
    }
    
    @IBAction func changeEmailButtonPressed(_ sender: UIButton) {
        changeEmail()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedField = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            changeEmail()
        }
        
        return true
    }
}
