//
//  LockViewController.swift
//  TestModalForm
//
//  Created by Amir Nazari on 5/30/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import Firebase

class LockViewController: UIViewController {

    var ref: DatabaseReference!
    
    var senderVC: UIViewController!
    var codeToMatch: String = ""
    var enteredCode: String = ""
    var passcodeMaxLength = 4
    
    var resetting = false
    
    var unwindSegueToRoomSelectionID = "unwindSegueToRoomSelection"
    
    var lockIcons: [UIView] = []
    var keypadButtons: [UIButton] = []
    
    var enterNumbers: [Int] = []
    
    @IBOutlet weak var lockEntry1: UIView!
    @IBOutlet weak var lockEntry2: UIView!
    @IBOutlet weak var lockEntry3: UIView!
    @IBOutlet weak var lockEntry4: UIView!
    @IBOutlet weak var lockEntry5: UIView!
    @IBOutlet weak var lockEntry6: UIView!
    
    @IBOutlet weak var keypad1: UIButton!
    @IBOutlet weak var keypad2: UIButton!
    @IBOutlet weak var keypad3: UIButton!
    @IBOutlet weak var keypad4: UIButton!
    @IBOutlet weak var keypad5: UIButton!
    @IBOutlet weak var keypad6: UIButton!
    @IBOutlet weak var keypad7: UIButton!
    @IBOutlet weak var keypad8: UIButton!
    @IBOutlet weak var keypad9: UIButton!
    @IBOutlet weak var keypad0: UIButton!
    @IBOutlet weak var keypadDelete: UIButton!
    @IBOutlet weak var keypadEnter: UIButton!
    
    @IBOutlet weak var lockScreenTitle: UILabel!
    
    var locked = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        
        if let currentUserID = FirebaseUserManager().getCurrentUser()?.uid {
            
            ref = Database.database().reference().child("\(currentUserID)")
        }
        
        lockScreenTitle.text = "Enter Game Master Pin"
        
        lockIcons = [lockEntry1, lockEntry2, lockEntry3, lockEntry4, lockEntry5, lockEntry6]
        
        keypadButtons = [keypad1, keypad2, keypad3, keypad4, keypad5, keypad6, keypad7, keypad8, keypad9, keypad0, keypadDelete, keypadEnter]
        
        for lock in lockIcons {
            lock.layer.cornerRadius = lock.bounds.size.height / 2
            lock.backgroundColor = .white
            lock.layer.borderColor = UIColor.lightGray.cgColor
            lock.layer.borderWidth = 3.0
        }
        
        if passcodeMaxLength == 4 {
            lockEntry5.isHidden = true
            lockEntry6.isHidden = true
        }
        
        for keypadButton in keypadButtons {
            keypadButton.layer.cornerRadius = keypadButton.bounds.size.height / 2
            keypadButton.applyDropShadow()
        }
        
        ref.child("GameMasterPin").observe(.value) { (snap) in
            if let code = snap.value as? String {
                self.codeToMatch = code
            }
            
            self.locked = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ref.child("GameMasterPin").removeAllObservers()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func keypadButtonPressed(_ sender: UIButton) {
        
        if locked {
            print("locked until code retrieval")
            return
        }
        
        if sender.tag == -1 {
            if enterNumbers.count >= 1 {
                print("Delete last keypad entry")
                enterNumbers.removeLast()
            } else {
                print("Nothing to delete")
            }
        } else {
            if enterNumbers.count < passcodeMaxLength {
                enterNumbers.append(sender.tag)
            } else {
                print("Max reached")
            }
        }
    
        updateIcons()
    }
    
    @IBAction func enterButtonPressed(_ sender: UIButton) {
        if enterNumbers.count == passcodeMaxLength {
            print("Submitting code")

            for number in enterNumbers {
                let stringValue = String(number)
                enteredCode.append(stringValue)
            }
            
            if codeToMatch.count == 0 || resetting {

                resetting = false
                codeToMatch = enteredCode
                if ref != nil {
                    ref.child("GameMasterPin").setValue(enteredCode)
                }

                let alert = UIAlertController(title: "Pin Set Successfully", message: "Game Master pin has been set!", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    self.dismiss(animated: true, completion: nil)
                }))
                
                self.present(alert, animated: true, completion: nil)
                
                return
            }
            
            if enteredCode == codeToMatch {
                print("Correct Code inputted")
                if let roomController = senderVC as? RoomController {
                    roomController.closeRoom()
                    performSegue(withIdentifier: unwindSegueToRoomSelectionID, sender: self)
                    return
                }
                
                if let _ = senderVC as? AppSettingsViewController {
                    enteredCode = ""
                    enterNumbers.removeAll()
                    updateIcons()
                    resetting = true
                    lockScreenTitle.text = "Enter NEW Game Master Pin"
                    return
                }
            } else {
                print("Wrong code entered")
                enteredCode = ""
                enterNumbers.removeAll()
                updateIcons()
            }
            
        } else {
            print("Please enter more numbers")
        }
    }
    
    func updateIcons() {
        // Update icon fill
        for count in 0...passcodeMaxLength {
            if count < enterNumbers.count {
                lockIcons[count].backgroundColor = UIColor.kiwiGreen()
            } else {
                lockIcons[count].backgroundColor = .white
            }
        }
    }
}
