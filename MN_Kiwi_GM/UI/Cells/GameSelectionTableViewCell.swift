//
//  GameSelectionTableViewCell.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 3/4/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit

class GameSelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var containerStack: UIStackView!
    @IBOutlet weak var roomButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
