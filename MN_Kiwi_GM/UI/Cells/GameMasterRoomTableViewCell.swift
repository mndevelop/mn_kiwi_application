//
//  GameMasterRoomTableViewCell.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 3/11/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit

class GameMasterRoomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timerBackgroundView: UIView!
    @IBOutlet weak var cellHeaderBackground: UIView!
    @IBOutlet weak var accessoryIndicatorLabel: UILabel!
    @IBOutlet weak var roomsConnectedLabel: UILabel!
    
    static let reuseID = "gameRoomCellIdentifier"
    
    @IBOutlet weak var roomTimerLabel: UILabel!
    @IBOutlet weak var roomNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
