//
//  RoomController.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 2/20/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import FirebaseDatabase
import MZTimerLabel
import AudioToolbox
import MediaPlayer
import AVFoundation

class RoomController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var clueImage1: UIImageView!
    @IBOutlet weak var clueImage2: UIImageView!
    @IBOutlet weak var clueImage3: UIImageView!
    
    @IBOutlet weak var clueImageBackground1: UIView!
    @IBOutlet weak var clueImageBackground2: UIView!
    @IBOutlet weak var clueImageBackground3: UIView!
    
    @IBOutlet weak var enterTeamNameField: UITextField!
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var timerBackgroundView: UIView!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var maintenanceButton: UIButton!
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var messageBackgroundView: UIView!
    @IBOutlet weak var timerBackgroundImageView: UIImageView!
    
    let player = MPMusicPlayerController.systemMusicPlayer
    
    // State Tracking
    var messagesReceived: [String] = []
    var clueImages: [UIImageView] = []
    var clueBackgrounds: [UIView] = []
    
    // Should have setting for Default team name
    var teamName = "Team Kiwi"
    var taps = 0
    
    // Room Object
    var room = ""
    var thisRoom : GameRoom!
    var timerRunning = false
    var currentHelpStatus = "Connected"
    var mzTimerLabel: MZTimerLabel!
    var songToPlay: MPMediaItem?
    var winTimeString: String = ""
    
    let clueButtonTitle = "CLUE"
    let maintenanceButtonTitle = "MAINTENANCE"
    
    var ref: DatabaseReference!
    var userRef: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        
        clueImages = [clueImage1, clueImage2, clueImage3]
        clueBackgrounds = [clueImageBackground1, clueImageBackground2, clueImageBackground3]
        
        timerBackgroundImageView.image = UIImage(named: thisRoom.themeImageName)
        messageBackgroundView.alpha = 0.9
        messageBackgroundView.layer.backgroundColor = UIColor.white.cgColor
        
        switch thisRoom.themeImageName {
        case "Default":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .darkGray
            
            enterTeamNameField.textColor = .lightGray
        case "timer1":
            break
        case "timer2":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .darkGray
            
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray])
        case "timer3":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .white
            
            enterTeamNameField.textColor = .lightGray
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        case "timer4":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = UIColor.kiwiGold()
            
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        case "timer5":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .darkGray
        case "timer6":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .white
            
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            break
        case "timer7":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .white
            
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            break
        case "timer8":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .white
            
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            break
        case "timer9":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .white
            
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            break
        case "timer10":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .darkGray
            
            enterTeamNameField.textColor = .darkGray
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray])
            break
        case "timer11":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .darkGray
            
            enterTeamNameField.textColor = .darkGray
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray])
            break
        case "timer12":
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            break
        case "timer13":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .white
            
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            break
        case "timer14":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .white
            
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            break
        case "timer15":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .white
            
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            break
        case "timer16":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .darkGray
            
            enterTeamNameField.textColor = .darkGray
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray])
        case "timer17":
            maintenanceButton.backgroundColor = .darkGray
            timerLabel.textColor = .darkGray
            
            enterTeamNameField.textColor = .darkGray
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray])
            
        case "timer18":
            maintenanceButton.backgroundColor = .kiwiNavyBlue()
            timerLabel.textColor = .kiwiNavyBlue()
            
            for clue in clueBackgrounds {
                clue.backgroundColor = .kiwiNavyBlue()
            }
            
            helpButton.backgroundColor = .kiwiPeterRiverBlue()
            
            enterTeamNameField.textColor = .darkGray
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray])
            
        case "timer19":
            maintenanceButton.backgroundColor = .kiwiNavyBlue()
            timerLabel.textColor = .kiwiNavyBlue()
            
            for clue in clueBackgrounds {
                clue.backgroundColor = .kiwiNavyBlue()
            }
            
            messageBackgroundView.alpha = 0.75
            
            helpButton.backgroundColor = .kiwiPeterRiverBlue()
            
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            
        case "timer20":
            maintenanceButton.backgroundColor = .kiwiNavyBlue()
            timerLabel.textColor = .kiwiNavyBlue()
            
            for clue in clueBackgrounds {
                clue.backgroundColor = .kiwiNavyBlue()
            }
            
            helpButton.backgroundColor = .kiwiPeterRiverBlue()
            
            enterTeamNameField.textColor = .darkGray
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray])
            
        case "timer21":
            maintenanceButton.backgroundColor = .kiwiNavyBlue()
            timerLabel.textColor = .white
            
            for clue in clueBackgrounds {
                clue.backgroundColor = .kiwiNavyBlue()
            }
            
            messageBackgroundView.alpha = 0.65
            messageBackgroundView.layer.backgroundColor = UIColor.kiwiScarletRed().cgColor
            
            helpButton.backgroundColor = .kiwiScarletRed()
            
            enterTeamNameField.textColor = .white
            enterTeamNameField.attributedPlaceholder = NSAttributedString(string: "Enter Team Name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            
        default:
            break
        }
        
        if thisRoom.songName != "" {
            let query: MPMediaQuery = MPMediaQuery()
            if let items = query.items {
                for song in items {
                    if let songTitle = song.value(forProperty: MPMediaItemPropertyTitle) as? String {
                        if thisRoom.songName == songTitle {
                            songToPlay = song
                            self.player.setQueue(with: MPMediaItemCollection(items: [songToPlay!]))
                            self.player.nowPlayingItem = songToPlay
                            self.player.repeatMode = .one
                        }
                    }
                }
            }
        }
        
        styleUI()
        
        for view in clueBackgrounds {
            view.layer.cornerRadius = 45
        }
        
        guard let userId = FirebaseUserManager().getCurrentUser()?.uid else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        ref = Database.database().reference().child(userId).child("Current Games").child(self.room)
        
        userRef = Database.database().reference().child(userId)
        
        // This is probably unnecessary but we don't want any wild observers running around.
        ref.removeAllObservers()
        
        ref.child("Win Time").observe(.value) { (snapshot) in
            if let winTime = (snapshot.value as? NSNumber) as? TimeInterval {
                self.timerRunning = false
                
                if let label = self.mzTimerLabel {
                    label.setCountDownTime(winTime)
                    label.pause()
                } else {
                    self.createTimer(withTime: winTime, forLabel: self.timerLabel)
                }
                
                self.winTimeString = self.mzTimerLabel.text ?? ""
            }
        }
        
        setupTimerRefForSelectedRoom()
        
        ref.child("helpStatus").observe(.value) { (snapshot) in
            
            if let helpStatus = snapshot.value as? String {
                
                DispatchQueue.main.async {
                    if helpStatus != "Disconnected" {
                        self.currentHelpStatus = helpStatus
                        self.updateHelpUI()
                    }
                }
            }
        }
        
        ref.child("playAlert").observe(.value) { (snapshot) in
            if let alertStatus = snapshot.value as? Int {
                if alertStatus == 1 {
                    AudioServicesPlayAlertSound(self.thisRoom.alertCode)
                }
            }
        }
        
        ref.child("roomMessages").observe(.value) { (snapshot) in
            
            self.messagesReceived = []
            
            for child in snapshot.children {
                
                guard let snap = child as? DataSnapshot else {
                    return
                }
                
                if let message = snap.value as? String {
                    self.messagesReceived.append(message)
                    
                    if self.currentHelpStatus == "Help"
                    {
                        self.ref.child("clueCount").observeSingleEvent(of: .value) { (snapshot) in
                            
                            var count = 1
                            
                            if let clueCount = snapshot.value as? NSNumber {
                                count = clueCount.intValue + count
                            }
                            
                            self.ref.child("clueCount").setValue(count)
                            self.thisRoom.cluesUsed = count
                        }
                    }
                }
            }
            
            DispatchQueue.main.async {
                if self.messagesReceived.count > 0 {
                    AudioServicesPlayAlertSound(self.thisRoom.alertCode)
                }
                
                self.messagesReceived = self.messagesReceived.reversed()
                self.messageTableView.reloadData()
            }
        }
        
        self.ref.child("clueCount").observe(.value) { (snapshot) in
            
            var count = 0
            
            if let clueCount = snapshot.value as? NSNumber {
                count = clueCount.intValue
            }
            
            self.thisRoom.cluesUsed = count
            
            DispatchQueue.main.async {
                // Maybe 4 should be like a max clue count or something? But there is only 3 images
                
                if count > 0 && count < 4 {
                    for num in 1...count {
                        self.clueImages[num - 1].tintColor = UIColor.kiwiGreen()
                    }
                } else if count >= 4 {
                    // make all images green
                    for image in self.clueImages {
                        image.tintColor = UIColor.kiwiGreen()
                    }
                } else {
                    for image in self.clueImages {
                        image.tintColor = .white
                    }
                }
            }
        }
        
        ref.child("connectedUserCount").observeSingleEvent(of: .value) { (snapshot) in
            
            var count = 1
            
            if let roomCount = snapshot.value as? NSNumber {
                count = roomCount.intValue + count
            }
            
            if count == 1 {
                if self.timerRunning {
                    self.ref.child("helpStatus").setValue("ConnectedTimerRunning")
                } else {
                    
                    if self.mzTimerLabel != nil {
                        let timeShowing = self.mzTimerLabel.getCountDownTime()
                        
                        if timeShowing == self.thisRoom.gameTimeLimit {
                            self.ref.child("helpStatus").setValue("Connected")
                        } else {
                            self.ref.child("helpStatus").setValue("ConnectedTimerPaused")
                        }
                    } else {
                        self.ref.child("helpStatus").setValue("Connected")
                    }
                }
            }
            
            self.ref.child("connectedUserCount").setValue(count)
            self.thisRoom.connectedRooms = count
        }
        
        ref.child("Team Name").observe(.value) { (snap) in
            guard let team = snap.value as? String else {
                DispatchQueue.main.async {
                    self.teamName = ""
                    self.enterTeamNameField.text = ""
                }
                return
            }
            
            self.teamName = team
            DispatchQueue.main.async {
                self.enterTeamNameField.text = self.teamName
            }
        }
    }
    
    public func closeRoom() {
        self.ref.child("GameMasterPin").removeAllObservers()
        self.ref.child("helpStatus").removeAllObservers()
        self.ref.child("connectedUserCount").removeAllObservers()
        self.ref.child("clueCount").removeAllObservers()
        self.ref.child("roomMessages").removeAllObservers()
        self.ref.child("Timer Info").removeAllObservers()
        self.ref.child("completionTime").removeAllObservers()
        self.ref.child("Win Time").removeAllObservers()
        self.ref.child("Team Name").removeAllObservers()
        ref.child("playAlert").removeAllObservers()
        
        DispatchQueue.main.async {
            
            if self.mzTimerLabel != nil {
                self.mzTimerLabel = nil
            }
            
            self.player.stop()
            
            self.ref.child("connectedUserCount").observeSingleEvent(of: .value) { (snapshot) in
                
                var count = 0
                
                if let roomCount = snapshot.value as? NSNumber {
                    count = roomCount.intValue - 1
                }
                
                if count <= 0 {
                    self.ref.child("helpStatus").setValue("Disconnected")
                }
                
                self.ref.child("connectedUserCount").setValue(max(0, count))
            }
        }
        
        print("unwound room successfully")
    }
    
    @IBAction func gameMasterRoomExitButtonPressed(_ sender: UIButton) {
        
        userRef.child("GameMasterPin").observe(.value) { (snap) in
            if let code = snap.value as? String {
                if code.count == 0 {
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        let storyBoard = UIStoryboard(name: "LockScreen", bundle: nil)
                        if let lockViewController = storyBoard.instantiateInitialViewController() as? LockViewController {
                            lockViewController.senderVC = self
                            self.present(lockViewController, animated: true, completion: nil)
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    
                    let alert = UIAlertController(title: "Close Room?", message: "Are you sure you would like to exit the game timer screen?", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.closeRoom()
                        self.navigationController?.popViewController(animated: true)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        
    }
    
    func updateHelpUI() {
        
        switch self.currentHelpStatus {
        case "Help":
            self.helpButton.setTitle("Help is on the way!", for: .normal)
        case "Maintenance":
            self.helpButton.setTitle(clueButtonTitle, for: .normal)
        case "ConnectedGameOver":
            self.helpButton.setTitle(clueButtonTitle, for: .normal)
            
            DispatchQueue.main.async {
                AudioServicesPlayAlertSound(1332)
            }
            
            let alert = UIAlertController(title: "Game Over", message: "Better luck next time...", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            self.thisRoom.gameOver = true
        case "ConnectedGameOverWINNER":
            self.helpButton.setTitle(clueButtonTitle, for: .normal)
            
            let winSoundCode: SystemSoundID = 1036
            
            DispatchQueue.main.async {
                AudioServicesPlayAlertSoundWithCompletion(winSoundCode, {
                    AudioServicesPlayAlertSoundWithCompletion(winSoundCode, {
                        AudioServicesPlayAlertSoundWithCompletion(winSoundCode, {
                            
                        })
                    })
                })
            }
            
            let alert = UIAlertController(title: "You Won!", message: "You have completed \(self.thisRoom.name) successfully!", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            self.thisRoom.gameOver = true
        case "Connected":
            fallthrough
        case "Disconnected":
            fallthrough
        default:
            self.helpButton.setTitle(clueButtonTitle, for: .normal)
        }
    }
    
    func setupTimerRefForSelectedRoom() {
        ref.child("Timer Info").observe(.value) { (snapshot) in
            
            guard let snap = snapshot.value as? [String : Any] else {
                
                if let song = self.songToPlay {
                    self.player.stop()
                    self.player.setQueue(with: MPMediaItemCollection(items: [song]))
                    self.player.nowPlayingItem = song
                    self.player.repeatMode = .one
                }
                
                DispatchQueue.main.async {
                    
                    if let mzTimer = self.mzTimerLabel {
                        mzTimer.pause()
                        self.timerRunning = false
                        mzTimer.setCountDownTime(self.thisRoom.gameTimeLimit)
                    } else {
                        self.createTimer(withTime: self.thisRoom.gameTimeLimit, forLabel: self.timerLabel)
                    }
                }
                return
            }
            
            if let running = snap["timerRunning"] as? NSNumber {
                
                if self.songToPlay != nil {
                    if running == 0 {
                        self.player.pause()
                    } else {
                        self.player.play()
                    }
                }
                
                DispatchQueue.main.async {
                    
                    guard let timeStamp = snap["timeStamp"] as? TimeInterval else { return }
                    guard let totalRunTime = snap["totalRunTime"] as? TimeInterval else { return }
                    
                    var timeToStart = totalRunTime - (Date().timeIntervalSince1970 - timeStamp)
                    
                    // TODO: REFACTOR THIS SHIZZZ
                    if running == 0 {
                        timeToStart = totalRunTime
                    }
                    
                    if let mzTimer = self.mzTimerLabel {
                        mzTimer.removeFromSuperview()
                    }
                    
                    self.timerLabel.text = ""
                    self.createTimer(withTime: timeToStart, forLabel: self.timerLabel)
                    
                    if running == 0 {
                        if let mzTimer = self.mzTimerLabel {
                            mzTimer.pause()
                            self.timerRunning = false
                        }
                    } else {
                        self.thisRoom.gameOver = false
                        
                        self.mzTimerLabel.start(ending: { (countTime) in
                            self.timerRunning = false
                            
                            if !self.thisRoom.gameOver {
                                self.ref.child("helpStatus").setValue("ConnectedGameOver")
                            }
                        })
                        self.timerRunning = true
                    }
                }
            }
        }
    }
    
    func styleUI() {
        helpButton.applyDropShadow()
        maintenanceButton.applyDropShadow()
        
        messageTableView.tableFooterView = UIView(frame: .zero)
        messageTableView.alwaysBounceVertical = false
        messageTableView.rowHeight = UITableViewAutomaticDimension
        messageTableView.estimatedRowHeight = 80
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func createTimer(withTime time: TimeInterval,
                     forLabel textLabel: UILabel) {
        mzTimerLabel = MZTimerLabel.init(label: textLabel, andTimerType: MZTimerLabelTypeTimer)
        mzTimerLabel.resetTimerAfterFinish = false
        mzTimerLabel?.setCountDownTime(time)
    }
    
    @IBAction func helpButtonPressed(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            ref.child("helpStatus").setValue("Help")
        case 1:
            ref.child("helpStatus").setValue("Maintenance")
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesReceived.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MessageTableViewCell.reuseID) as? MessageTableViewCell else {
            return UITableViewCell()
        }
        
        if self.thisRoom.themeImageName == "timer21" {
            cell.backgroundColor = UIColor.kiwiScarletRed()
            cell.messageLabel.textColor = .white
            
        }
        
        cell.messageLabel.text = messagesReceived[indexPath.row]
        
        return cell
    }
}

extension RoomController {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let enteredTeamName = textField.text else { return }
        
        if teamName == enteredTeamName {
            return
        }
        
        ref.child("Team Name").setValue(enteredTeamName)
        return
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        enterTeamNameField.resignFirstResponder()
        return true
    }
}

