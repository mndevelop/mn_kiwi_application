//
//  RoomSelectionViewController.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 2/24/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import Lottie

class RoomSelectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var animationStackViewContainer: UIStackView!
    @IBOutlet weak var animationContainerView: UIView!
    var animationView: LOTAnimationView!
    
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet var roomSelectionOptionButtons: [UIButton]!
    @IBOutlet weak var addGameButton: UIButton!
    @IBOutlet weak var editGameButton: UIButton!
    
    @IBOutlet weak var roomSelectionTableView: UITableView!
    @IBOutlet weak var selectionTableBackgroundView: UIView!
    
    var editingGames = false
    
    var selectedRoom: GameRoom!
    var gameList: [GameRoom] = []
    
    var handle: AuthStateDidChangeListenerHandle!
    
    var ref: DatabaseReference!
    
    var gameLimit = 0
    
    var indicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleUI()
        
        emptyStateLabel.text = "No Games To Display. \nTap ADD GAME Below \nTo Create A Game."
        
        if let currentUserID = FirebaseUserManager().getCurrentUser()?.uid {
            
            ref = Database.database().reference().child("\(currentUserID)")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if ref == nil {
            self.navigationController?.dismiss(animated: true, completion: {
                // Display alert to user?
            })
            
            return 
        }
        
        activityIndicator()
        indicator.startAnimating()
        setupGameObservers()
        emptyStateAnimation()
        
        if gameList.count <= 0 {
            if let animatedView = animationView {
                animatedView.play()
            } else {
                playAnimation(view: animationView)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        removeFirebaseObservers()
        
        if let animatedView = animationView {
            if animatedView.isAnimationPlaying {
                animatedView.stop()
            }
            
            for view in animationContainerView.subviews {
                view.removeFromSuperview()
            }
            
            animationView = nil
        }
    }
    
    func activityIndicator() {
            indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            indicator.activityIndicatorViewStyle = UIActivityIndicatorView.Style.whiteLarge
            indicator.color = .white
            indicator.center = self.roomSelectionTableView.center
            self.view.addSubview(indicator)
    }
    
    func removeFirebaseObservers() {
        ref.child("Game Limit").removeAllObservers()
        ref.child("Games").removeAllObservers()
    }
    
    func setupGameObservers() {
        ref.child("Game Limit").observe(.value, with: { (snapshot) in
            
            if let count = snapshot.value as? NSNumber {
                self.gameLimit = count.intValue
            }
            
        })
        
        ref.child("Games").observe(.value, with: { (snapshot) in
            //if the reference have some values
            
            if snapshot.childrenCount > 0 {
                
                //clearing the list
                self.gameList.removeAll()
                
                //iterating through all the values
                for child in snapshot.children {
                    if let value = (child as? DataSnapshot)?.value as? [String : Any] {
                        
                        let gameRoom = GameRoom()
                        
                        if let snap = child as? DataSnapshot {
                            gameRoom.key = snap.key
                        }
                        
                        guard let name = value["name"] as? String else {
                            return
                        }
                        
                        gameRoom.name = name
                        
                        if let timeLimit = value["timeLimit"] as? NSNumber {
                            gameRoom.gameTimeLimit = TimeInterval(truncating: timeLimit)
                        } else {
                            gameRoom.gameTimeLimit = 3600
                        }
                        
                        if let songName = value["songName"] as? String {
                            gameRoom.songName = songName
                        }
                        
                        if let alertID = value["alertID"] as? UInt32 {
                            gameRoom.alertCode = alertID
                        }
                        
                        if let themeImage = value["themeImage"] as? String {
                            gameRoom.themeImageName = themeImage
                        }
                        
                        self.gameList.append(gameRoom)
                    }
                }
                
                DispatchQueue.main.async {
                    self.animationStackViewContainer.isHidden = true
                    self.roomSelectionTableView.isHidden = false
                }
            } else {
                self.gameList = []
                
                DispatchQueue.main.async {
                    if let animatedView = self.animationView {
                        animatedView.play()
                    } else {
                        self.playAnimation(view: self.animationView)
                    }
                    self.animationStackViewContainer.isHidden = false
                    self.roomSelectionTableView.isHidden = true
                    if self.editingGames {
                        self.editingGames = false
                    }
                    
                    self.editGameButton.setTitle("Edit", for: .normal)
                }
            }
            
            DispatchQueue.main.async {
                //reloading the tableview
                for button in self.roomSelectionOptionButtons {
                    button.isEnabled = true
                }
                
                self.roomSelectionTableView.reloadData()
                self.indicator.stopAnimating()
            }
        })
    }
    
    func playAnimation(view: LOTAnimationView?) {
        if let animatedView = view {
            animatedView.play()
        }
    }
    
    func emptyStateAnimation() {
        let randomAnimation = "mr_lama_stikers__suspect"
        animationView = LOTAnimationView(name: randomAnimation)
        animationView.loopAnimation = true
        animationView.contentMode = .scaleAspectFit
        animationView.frame = CGRect(x: 0, y: 0, width: animationContainerView.bounds.width, height: animationContainerView.bounds.height)
        
        animationContainerView.addSubview(animationView)
    }
    
    func styleUI() {
        self.view.layoutIfNeeded()
        ApplyStyleToButtons()
        selectionTableBackgroundView.applyDropShadow(radius: 0)
        
        roomSelectionTableView.separatorStyle = .none
        roomSelectionTableView.layer.cornerRadius = 6.0
        
        roomSelectionTableView.tableFooterView = UIView(frame: .zero)
        roomSelectionTableView.alwaysBounceVertical = false
    }

    func ApplyStyleToButtons() {
        for button in roomSelectionOptionButtons {
            button.applyDropShadow()
            button.isEnabled = false
        }
        
        addGameButton.applyDropShadow()
        editGameButton.applyDropShadow()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showErrorAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Actions
    @IBAction func addGameButtonPressed(_ sender: UIButton) {
        
        if gameList.count < gameLimit {
            // Open add game form
            if editingGames {
                editingGames = false
            }
            
            editGameButton.setTitle("Edit", for: .normal)
            roomSelectionTableView.reloadData()
            
            self.performSegue(withIdentifier: "toAddFormVC", sender: nil)
        } else {
            showErrorAlert(title: "Game Maximum Reached", message: "Your account is out of games. Your game limit is: \(gameLimit).")
        }
    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        
        if gameList.count == 0 { return }
        
        // Editing games
        editingGames = !editingGames
        if editingGames {
            editGameButton.setTitle("Done", for: .normal)
        } else {
            editGameButton.setTitle("Edit", for: .normal)
        }
        
        roomSelectionTableView.reloadData()
    }
    
    @IBAction func gameMasterButtonPressed(_ sender: UIButton) {
        // Navigate to game master screen
        
        if gameList.count > 0 {
            self.performSegue(withIdentifier: "toGameMasterVC", sender: nil)
        } else {
            
        }
    }
    
    @IBAction func leaderboardButtonPressed(_ sender: UIButton) {
        // Open Leaderboards
    }
    
    @IBAction func settingsButtonPressed(_ sender: UIButton) {
        // Open settings
        self.performSegue(withIdentifier: "toSettingsVC", sender: self)
    }
    
    @IBAction func sendFeedbackAction(_ sender: UIButton) {
        // Open form or email client for user feedback entry
    }
    
    @IBAction func signOutAction(_ sender: UIButton) {
        //  Show notification asking if user wants to logout
        if Auth.auth().currentUser != nil {
            do {
            
                if skippedLogin {
                    // instantiate login vc and add on navigation controller completion?
                    let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
                    guard let loginVC = storyboard.instantiateInitialViewController() else {
                        // error logging out
                        return
                    }
                    
                    if let email = Auth.auth().currentUser?.email {
                        (loginVC as? LoginViewController)?.userEmail = email
                    }
                    
                    do {
                        try Auth.auth().signOut()
                    } catch {
                        print("Error signing out")
                    }
                    
                    UIApplication.shared.keyWindow?.rootViewController = loginVC
                } else {
                    
                    do {
                        try Auth.auth().signOut()
                    } catch {
                        print("Error signing out")
                    }
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gameList.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "gameRoomCellIdentifier") as? GameSelectionTableViewCell else {
            return UITableViewCell()
        }
        
        if self.editingGames {
            cell.deleteButton.isHidden = false
            let yellowColor = UIColor(red: 236/255, green: 250/255, blue: 58/255, alpha: 1.0)
            cell.roomButton.layer.backgroundColor = yellowColor.cgColor
        } else {
            cell.deleteButton.isHidden = true
            let greenColor = UIColor(red: 193/255, green: 220/255, blue: 74/255, alpha: 1.0)
            cell.roomButton.layer.backgroundColor = greenColor.cgColor
        }
        
        cell.roomButton.tag = indexPath.row
        cell.deleteButton.tag = indexPath.row
        cell.roomButton.setAttributedTitle(NSAttributedString.init(string: gameList[indexPath.row].name), for: .normal)
        
        cell.roomButton.addTarget(self, action: #selector(roomButtonTapped(sender:)), for: .touchUpInside)
        cell.deleteButton.addTarget(self, action: #selector(deleteButtonTapped(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    @objc func deleteButtonTapped(sender: UIButton) {
        selectedRoom = gameList[sender.tag]
        
        let alert = UIAlertController(title: "Delete \(selectedRoom.name)?", message: "This will PERMANENTLY DELETE \(selectedRoom.name) and all of it's data. \n\nAre you sure you want to do this?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes, Delete this game", style: .destructive, handler: { (alert) in
            self.ref.child("Games").child(self.selectedRoom.key).removeValue()
            self.ref.child("Current Games").child(self.selectedRoom.name).removeValue()
            self.ref.child("Clue Lists").child(self.selectedRoom.name).removeValue()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
        
    @objc func roomButtonTapped(sender: UIButton) {
        selectedRoom = gameList[sender.tag]
        
        if editingGames {
            self.performSegue(withIdentifier: "toAddFormVC", sender: nil)
        } else {
            self.performSegue(withIdentifier: "toRoomVC", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        editGameButton.setTitle("Edit", for: .normal)
        roomSelectionTableView.reloadData()
        
        if let roomVC = segue.destination as? RoomController {
            roomVC.thisRoom = selectedRoom
            roomVC.room = selectedRoom.name
            return
        }
        
        if let addForm = segue.destination as? AddRoomFormViewController {
            if editingGames {
                addForm.gameToEdit = self.selectedRoom
            }
            
            addForm.gameList = self.gameList
        }
        
        if let settingsVC = segue.destination as? AppSettingsViewController {
            settingsVC.gameList = gameList
        }
        
        if let gameMasterVC = segue.destination as? GameMasterController {
            if gameList.count >= 1 {
                let currentGame = gameList[0]
                gameMasterVC.selectedRoom = currentGame
            }
        }
        
        editingGames = false
    }
    
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) {
        print("Made it back successfully")
    }
}
