//
//  ThemeSelectionCollectionViewCell.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 5/5/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit

class ThemeSelectionCollectionViewCell: UICollectionViewCell {
    
    public static let reuseID = "themeCellReuseID"
    
    @IBOutlet weak var themeImage: UIImageView!
    @IBOutlet weak var themeTitleLabel: UILabel!
}
