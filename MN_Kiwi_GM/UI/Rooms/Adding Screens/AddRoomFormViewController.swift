//
//  AddRoomFormViewController.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 2/24/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import FirebaseDatabase
import MediaPlayer
import AVFoundation

class AddRoomFormViewController: UIViewController, MPMediaPickerControllerDelegate {
    
    @IBOutlet weak var formBackgroundView: UIView!
    
    @IBOutlet weak var gameNameTextField: UITextField!
    @IBOutlet weak var musicFileTextField: UITextField!
    @IBOutlet weak var getMusicFromLibraryButton: UIButton!
    @IBOutlet weak var chooseAlertSoundButton: UIButton!
    @IBOutlet weak var alertTitleTextField: UITextField!
    @IBOutlet weak var gameTimePicker: UIDatePicker!
    @IBOutlet weak var saveChangesButton: UIButton!
    @IBOutlet weak var closeWithoutSavingButton: UIButton!
    @IBOutlet weak var themeSelectionCollectionView: UICollectionView!
    
    var mediaPicker: MPMediaPickerController?
    
    var gameList: [GameRoom] = []
    var gameToEdit: GameRoom?
    var originalName = ""
    
    var selectedTheme = ""
    var selectedAlert: UInt32 = 1021
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Change this if default theme changes
        selectedTheme = "Default"
        
        if let game = gameToEdit {
            selectedTheme = game.themeImageName
            originalName = game.name
            gameNameTextField.text = game.name
            musicFileTextField.text = game.songName
            alertTitleTextField.text = "\(game.alertCode)"
            gameTimePicker.countDownDuration = game.gameTimeLimit
        }
        
        // Do any additional setup after loading the view.
        styleUI()
    }

    func styleUI() {
        self.view.layoutIfNeeded()
        gameNameTextField.applyDropShadow()
        musicFileTextField.applyDropShadow()
        getMusicFromLibraryButton.applyDropShadow()
        chooseAlertSoundButton.applyDropShadow()
        alertTitleTextField.applyDropShadow()
        saveChangesButton.applyDropShadow()
        closeWithoutSavingButton.applyDropShadow()
        formBackgroundView.applyDropShadow()
        
        gameTimePicker.setValue(UIColor.white, forKey: "textColor")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveGameButtonPressed(_ sender: Any) {
        //  Save game to firebase with some user info.
        if let currentUserID = FirebaseUserManager().getCurrentUser()?.uid {
            
            if let gameNameText = gameNameTextField.text {
    
                var trimmedString = gameNameText.trimmingCharacters(in: .whitespaces)
                trimmedString = trimmedString.removingCharacters(inCharacterSet: .punctuationCharacters)
                trimmedString = trimmedString.removingCharacters(inCharacterSet: .illegalCharacters)
                
                if trimmedString.count <= 0 {
                    let alert = UIAlertController(title: "Game Name is empty", message: "You cannot save a game without a name.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                // set time limit
                var game = ["name" : trimmedString, "timeLimit" : gameTimePicker.countDownDuration] as [String : Any]
                
                if let musicName = self.musicFileTextField.text {
                    game["songName"] = musicName
                 }
                
                game["alertID"] = selectedAlert
        
                game["themeImage"] = selectedTheme
                
                if let editGame = gameToEdit {
                    Database.database().reference().child("\(currentUserID)").child("Current Games").child(originalName).removeValue()
                    Database.database().reference().child("\(currentUserID)").child("Games").child(editGame.key).setValue(game)
                    let editSuccessAlert = UIAlertController(title: "Game Edited Successfully", message: "You will need to reopen any game rooms running \(originalName) in order to see the changes reflected.", preferredStyle: .alert)

                    editSuccessAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    
                    let editConfirmAlert = UIAlertController(title: "Save Changes", message: "Saving changes will STOP any games running \(originalName). You will need to close and reopen any game rooms running \(originalName) in order to see the changes reflected.", preferredStyle: .alert)

                    editConfirmAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    
                    editConfirmAlert.addAction(UIAlertAction(title: "Save Changes", style: .default, handler: { (alert) in
                        self.present(editSuccessAlert, animated: true, completion: nil)
//                        self.dismiss(animated: true, completion:   {
//
//                        })
                    }))
                    
                    self.present(editConfirmAlert, animated: true, completion: nil)
                    
                } else {
                    
                    if listContainsGameTitle(title: trimmedString, gameList: gameList) {
                        
                        let alert = UIAlertController(title: "\(trimmedString) Already Exists", message: "You cannot save more than one game with the same name.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        return
                    }
                    
                    Database.database().reference().child("\(currentUserID)").child("Games").childByAutoId().setValue(game)
                
                    let alert = UIAlertController(title: "Game Saved", message: "\(trimmedString) has been saved successfully! Tap OK to return to the game selection screen.", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                 
            } else {
                // Alert user that name must be entered
            }
        }
    }
    
    func listContainsGameTitle(title: String, gameList: [GameRoom]) -> Bool {
        
        for game in gameList {
            if game.name == title {
                return true
            }
        }
        
        return false
    }
    
    @IBAction func getMusicButtonPressed(_ sender: UIButton) {
        // Open music library to select song associated with game
        mediaPicker = MPMediaPickerController(mediaTypes: .anyAudio)
        
        if let picker = mediaPicker {
            
            print("Successfully instantiated a media picker")
            picker.delegate = self
            self.present(picker, animated: true, completion: nil)
            
        } else {
            print("Could not instantiate a media picker")
        }
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        mediaPicker.dismiss(animated: true, completion: nil)
    }
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {

        if let mediaTitle = mediaItemCollection.items.first?.title {
            self.musicFileTextField.text = mediaTitle
        } else {
            let alert = UIAlertController(title: "Media Could Not Load", message: "Please try again. If this persists, please try a different file format.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        mediaPicker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeFormButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AddRoomFormViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ThemeSelectionCollectionViewCell.reuseID, for: indexPath) as? ThemeSelectionCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        if selectedTheme == themeImageNames[indexPath.row] {
            cell.layer.backgroundColor = UIColor.kiwiGreen().cgColor
        } else {
            cell.layer.backgroundColor = UIColor.white.cgColor
        }
        
        cell.themeImage.image = UIImage(named: themeImageNames[indexPath.row] + "thumbnail")
        cell.themeTitleLabel.text = themes[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return themes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedTheme = themeImageNames[indexPath.row]
        collectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let alertVC = segue.destination as? SelectAlertTableViewController {
            alertVC.addRoomController = self
        }
    }
    
    @IBAction func unwindToAddRoom(segue:UIStoryboardSegue) {
        print("Made it back successfully")
        
        DispatchQueue.main.async {
            self.alertTitleTextField.text = "\(self.selectedAlert)"
        }
    }
}
