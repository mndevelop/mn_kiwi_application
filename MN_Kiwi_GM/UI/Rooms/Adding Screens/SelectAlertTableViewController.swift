//
//  SelectAlertTableViewController.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 6/24/19.
//  Copyright © 2019 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import AudioToolbox
import MediaPlayer
import AVFoundation

class SelectAlertTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var alertTableView: UITableView!
    @IBOutlet weak var cancelButton: UIButton!
    var addRoomController : AddRoomFormViewController!
    
    var unwindID = "unwindToAddRoom"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertTableView.tableFooterView = UIView(frame: .zero)
        
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 19
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cellReuse")
        
        cell.textLabel?.text = "Alert \(indexPath.row + 1)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.allowsSelection = false
        cancelButton.isEnabled = false
        
        // Select alert
        addRoomController.selectedAlert = systemSoundIds[indexPath.row]
        // Play sample
        AudioServicesPlayAlertSoundWithCompletion(systemSoundIds[indexPath.row], {
            self.performSegue(withIdentifier: self.unwindID, sender: self)
        })
        
    }
}
