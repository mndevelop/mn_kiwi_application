//
//  AddClueViewController.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 3/16/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import Firebase

class AddClueViewController: UIViewController {

    @IBOutlet weak var sectionTextField: UITextField!
    @IBOutlet weak var gameStepTextField: UITextField!
    @IBOutlet weak var hintForGameStepTextField: UITextField!
    @IBOutlet weak var saveClueButton: UIButton!

    var clueToEdit: [String : String]?
    var gameMasterVC: GameMasterController!
    
    var editingClue = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let clue = clueToEdit {
            gameStepTextField.text = clue["step"]
            hintForGameStepTextField.text = clue["hint"]
            editingClue = true
        }
        self.view.layoutIfNeeded()
        
        saveClueButton.applyDropShadow()
        
        gameStepTextField.applyDropShadow()
        hintForGameStepTextField.applyDropShadow()
        sectionTextField.applyDropShadow()
        
        gameStepTextField.attributedPlaceholder = NSAttributedString(string: "Game Step",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        hintForGameStepTextField.attributedPlaceholder = NSAttributedString(string: "Hint for Game Step",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        gameStepTextField.textColor = UIColor.darkGray
        hintForGameStepTextField.textColor = UIColor.darkGray
        
        // This will dismiss the add controller whenever a new game is selected.
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init("selectedRoomChanged"), object: nil, queue: OperationQueue.main) { (notification) in
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func saveClueButtonPressed(_ sender: UIButton) {
        
        guard let gameStep = gameStepTextField.text else { return }
        guard let hintForStep = hintForGameStepTextField.text else { return }
        
        if !gameStep.isEmpty && !hintForStep.isEmpty {
           
            let clueObject: [String : String] = ["section" : sectionTextField.text!, "step" : gameStep, "hint" : hintForStep, "completed" : "no"]
            
            gameStepTextField.text = ""
            hintForGameStepTextField.text = ""
            
            if editingClue {
                
                if let clue = clueToEdit {
                    if let key = clue["key"] {
                        gameMasterVC.ref.child("Clue Lists").child(gameMasterVC.selectedRoom.name).child(key).setValue(clueObject)
                        return
                    }
                }
            }
            
            gameMasterVC.ref.child("Clue Lists").child(gameMasterVC.selectedRoom.name).childByAutoId().setValue(clueObject)
            
        } else {
            let alert = UIAlertController(title: "Missing Fields", message: "Please fill out all the required information.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            gameMasterVC.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func addClueBackButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
