//
//  GameMasterController.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 2/20/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import Firebase
import MZTimerLabel
//import MZTimerLabel
import AudioToolbox

class GameMasterController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // Room Object
    var selectedRoom = GameRoom()
    var selectedRoomIndex = 0
    var selectedRoomTimerRunning = false
    var mzTimerLabel: MZTimerLabel!
    var totalTimeTimerLabel: MZTimerLabel!
    
    var chatMessagesForRoom: [String] = []
    
    var ref: DatabaseReference!
    
    @IBOutlet weak var timePickerView: UIView!
    @IBOutlet weak var timePicker: UIDatePicker!
    
    @IBOutlet var timerPanelButtons: [UIButton]!
    @IBOutlet weak var selectedRoomTimerBackground: UIView!
    @IBOutlet weak var selectedRoomNameLabel: UILabel!
    @IBOutlet weak var selectedRoomTimerLabel: UILabel!
    @IBOutlet weak var selectedRoomTotalTimeLabel: UILabel!
    @IBOutlet weak var cluesUsedLabel: UILabel!
    @IBOutlet weak var roomNameHeaderBackground: UIView!
    
    @IBOutlet weak var masterLockToggleSwitch: UISwitch!
    @IBOutlet weak var masterLockOverlayView: UIView!
    @IBOutlet weak var headerBackground: UIView!
    @IBOutlet weak var returnMenuButton: UIButton!
    @IBOutlet weak var timerButton: UIButton!
    @IBOutlet weak var chatTextField: UITextField!
    @IBOutlet weak var sendChatButton: UIButton!
    @IBOutlet weak var alertSoundButton: UIButton!
    @IBOutlet weak var roomSelectionTableView: UITableView!
    
    var gameList: [GameRoom] = []
    
    var helpSound: SystemSoundID = 1028
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let userId = FirebaseUserManager().getCurrentUser()?.uid else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        styleUI()
        selectedRoomNameLabel.text = selectedRoom.name
        
        // Firebase reference
        ref = Database.database().reference().child(userId)
        
        setUpGameRoomListListener()
    }
    
    func styleUI() {
        
        for button in timerPanelButtons {
            button.applyDropShadow()
        }
        
        selectedRoomTimerBackground.applyDropShadow()
        
        roomNameHeaderBackground.applyDropShadow(radius: 0)
        
        timePicker.setValue(UIColor.darkGray, forKeyPath: "textColor")
        

        chatTextField.applyDropShadow()
        chatTextField.attributedPlaceholder = NSAttributedString(string: "Game Step",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        chatTextField.textColor = UIColor.darkGray
        
        sendChatButton.applyDropShadow()
        
        alertSoundButton.applyDropShadow()
        
        roomSelectionTableView.alwaysBounceVertical = false
    }
    
    func removeAllObservers() {
        
        for game in gameList {
            ref.child("Current Games").child(game.name).child("connectedUserCount").removeAllObservers()
            ref.child("Current Games").child(game.name).child("helpStatus").removeAllObservers()
            ref.child("Current Games").child(game.name).child("roomMessages").removeAllObservers()
            ref.child("Current Games").child(game.name).child("Timer Info").removeAllObservers()
            ref.child("Current Games").child(game.name).child("Win Time").removeAllObservers()
            ref.child("Current Games").child(game.name).child("clueCount").removeAllObservers()
            ref.child("Clue Lists").child(game.name).removeAllObservers()
            ref.child("Current Games").child(game.name).removeAllObservers()
        }
        
        ref.child("Current Games").removeAllObservers()
        ref.child("Games").removeAllObservers()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let clueNavVC = segue.destination as? UINavigationController {
            if let clueVC = clueNavVC.topViewController as? ClueTableViewController {
                clueVC.gameMasterVC = self
            }
        }
    }
    
    @IBAction func returnToMenuButtonPressed(_ sender: UIButton) {
        removeAllObservers()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendAlertSound(_ sender: Any) {
        ref.child("Current Games").child(selectedRoom.name).child("playAlert").setValue(1)
        ref.child("Current Games").child(selectedRoom.name).child("playAlert").setValue(0)
    }
    
    @IBAction func sendChat(_ sender: Any) {
        // Send message from chatTextField
        
        guard let textMessage = chatTextField.text else {
            return
        }
        
        if textMessage.isEmpty {
            // Alert user that message field is empty
            return
        } else if !selectedRoom.isConnected {
            showRoomNotConnected()
            return
        }
        
        ref.child("Current Games").child(selectedRoom.name).child("roomMessages").childByAutoId()
        .setValue(chatTextField.text)
        
        if selectedRoomTimerRunning {
            ref.child("Current Games").child(selectedRoom.name).child("helpStatus").setValue("ConnectedTimerRunning")
        } else {
            ref.child("Current Games").child(selectedRoom.name).child("helpStatus").setValue("ConnectedTimerPaused")
        }
        
        // Clear chat after sending
        chatTextField.text = ""
    }
    
    func showClueText(message: String) {
        chatTextField.text = message
    }
    
    func setTimerButton() {
        if selectedRoomTimerRunning == true {
            timerButton.setTitle("Stop", for: .normal)
        } else {
            timerButton.setTitle("Start", for: .normal)
        }
    }
    
    @IBAction func reportWinButtonPressed(_ sender: UIButton) {
    
        if !selectedRoom.isConnected {
            showRoomNotConnected()
            return
        }
        // Show alert to confirm
        let alert = UIAlertController(title: "Report Win for \(selectedRoom.name)", message: "This will alert the player's that they won the game! \n\nAre you sure you want to do this?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Confirm Win", style: .default, handler: { (alert) in
            // Report win to room object
            // WINNNER WINNNER CHICKEN DINNER!
            self.sendWin()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendWin(shouldSendWinStatus: Bool = true) {
        if let timerLabel = self.mzTimerLabel {
            let remainingTime = timerLabel.getCountDownTime()
            self.selectedRoomTimerRunning = false
            DispatchQueue.main.async {
                self.setTimerTime(game: self.selectedRoom, timerRunning: self.selectedRoomTimerRunning)
            }
            if shouldSendWinStatus {
                self.ref.child("Current Games").child(self.selectedRoom.name).child("Win Time").setValue(remainingTime, andPriority: nil, withCompletionBlock: { (error, ref) in
                    
                    self.ref.child("Current Games").child(self.selectedRoom.name).child("helpStatus").setValue("ConnectedGameOverWINNER")
                    
                })
            }
            
            // Store win time in firebase to display on room controllers so that they all contain the same win time.
            // Consider making a firebase function for this to calculate final time in server and distribute to all devices.
            // Log win time from Game Master
        }
    }
    
    @IBAction func clearHelpRequestPressed(_ sender: UIButton) {
        
        if !selectedRoom.isConnected {
            showRoomNotConnected()
            return
        }
        
        let alert = UIAlertController(title: "Reset Help Status for \(selectedRoom.name)", message: "This will remove any maintenance or clue requests. This DOES NOT reset player's used clues. \n\nAre you sure you want to do this?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Clear Status", style: .default, handler: { (alert) in
            
            if self.selectedRoomTimerRunning {
                self.ref.child("Current Games").child(self.selectedRoom.name).child("helpStatus").setValue("ConnectedTimerRunning")
            } else {
                
                if let timerLabel = self.mzTimerLabel {
                    let timeShowing = timerLabel.getCountDownTime()
                    
                    if timeShowing == self.selectedRoom.gameTimeLimit {
                        self.ref.child("Current Games").child(self.selectedRoom.name).child("helpStatus").setValue("Connected")
                        return
                    }
                }
                
                self.ref.child("Current Games").child(self.selectedRoom.name).child("helpStatus").setValue("ConnectedTimerPaused")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func clearChatButtonPressed(_ sender: UIButton) {
        
        if !selectedRoom.isConnected {
            showRoomNotConnected()
            return
        }
        
        let alert = UIAlertController(title: "Reset Chat for \(selectedRoom.name)", message: "This will clear all chat messages that are visible in the room. If the timer is running, it will continue to run.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Clear Chat", style: .default, handler: { (alert) in
            
            self.ref.child("Current Games").child(self.selectedRoom.name).child("roomMessages").removeValue()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func timerButtonPressed(_ sender: UIButton) {
        
        if self.selectedRoom.currentHelpStatus == "ConnectedGameOver" || self.selectedRoom.currentHelpStatus == "ConnectedGameOverWINNER"{
            
            self.clearClueCompletion()
            self.ref.child("Current Games").child(self.selectedRoom.name).removeValue()
            self.ref.child("Current Games").child(self.selectedRoom.name).child("connectedAccessoryCount").setValue(self.selectedRoom.connectedAccessories)
            self.ref.child("Current Games").child(self.selectedRoom.name).child("connectedUserCount").setValue(self.selectedRoom.connectedRooms)
            
            self.totalTimeTimerLabel.setStopWatchTime(self.selectedRoom.gameTimeLimit)
            self.mzTimerLabel.setCountDownTime(self.selectedRoom.gameTimeLimit)
        }
        
        // Check if current selected room has timer connected
        if self.selectedRoom.isConnected {
            
            self.selectedRoomTimerRunning = !self.selectedRoomTimerRunning
            self.setTimerTime(game: self.selectedRoom, timerRunning: self.selectedRoomTimerRunning)
        } else {
            showRoomNotConnected()
        }
    }
    
    func setTimerTime(game: GameRoom, timerRunning: Bool) {
        var number : NSNumber = 0
        var totalRunTime : TimeInterval = 0.0
        
        if game.key == selectedRoom.key {
            if timerRunning == true {
                number = 1
                
                // Create Timer with start time
                if let timerLabel = self.mzTimerLabel {
                    totalRunTime = timerLabel.getTimeRemaining()
                } else {
                    createTimer(withTime: game.gameTimeLimit, forLabel: selectedRoomTimerLabel)
                    totalRunTime = mzTimerLabel.getTimeRemaining()
                }
            } else {
                
                if let timerLabel = self.mzTimerLabel {
                    totalRunTime = timerLabel.getTimeRemaining()
                }
            }
        
            if game.currentHelpStatus == "ConnectedTimerRunning" || game.currentHelpStatus == "ConnectedTimerPaused" || game.currentHelpStatus == "Connected" || game.currentHelpStatus == "ConnectedGameOver" {
                if timerRunning {
                    self.ref.child("Current Games").child(game.name).child("helpStatus").setValue("ConnectedTimerRunning")
                } else {
                    self.ref.child("Current Games").child(game.name).child("helpStatus").setValue("ConnectedTimerPaused")
                }
            }
        }
        
        // This sets a timestamp and total running time for the timer. This will help the other devices determine the timer information by taking the difference between the timestamp and the current time. The formula looks like this:
        // Start Time -> timeStamp, totalRunTime
        // RequestTime (Room Controller or Additional Game Master) -> totalRunTime - (currentTime - timeStamp)  = timeToShow
        
        // The totalRunTime with current timer time for room.
        let timer : [String : Any] = ["timerRunning" : number, "timeStamp" : Date().timeIntervalSince1970, "totalRunTime" : totalRunTime]
        
        self.ref.child("Current Games").child(game.name).child("Timer Info").setValue(timer)
    }
    
    func createTimer(withTime time: TimeInterval,
                     forLabel textLabel: UILabel) {
        
        let inverseTime = selectedRoom.gameTimeLimit - time
        self.mzTimerLabel = MZTimerLabel.init(label: textLabel, andTimerType: MZTimerLabelTypeTimer)
        self.mzTimerLabel.resetTimerAfterFinish = false
        self.totalTimeTimerLabel = MZTimerLabel.init(label: self.selectedRoomTotalTimeLabel, andTimerType: MZTimerLabelTypeStopWatch)
        
        self.mzTimerLabel?.setCountDownTime(time)
        self.totalTimeTimerLabel.setStopWatchTime(inverseTime)
    }
    
    @IBAction func setCustomRoomTimeButtonPressed(_ sender: UIButton) {
        // Open set timer time modal window
        // Set currentTimeProperty on room object (Firebase)
        // Update UI on Game Master to reflect changes
        
        if selectedRoomTimerRunning {
            let alert = UIAlertController(title: "Set time for \(selectedRoom.name)", message: "This will pause the current game. \n\nAre you sure you want to continue?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (alert) in
                self.openTimePicker()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)

        } else {
            openTimePicker()
        }
    }
    
    func openTimePicker() {
        // THIS IS THE SAME LOGIC AS TIMER BUTTON PRESSED. Except it just sets timer running to false
        // Check if current selected room has timer connected
        if self.selectedRoom.isConnected {
            
            self.selectedRoomTimerRunning = false
            self.setTimerTime(game: self.selectedRoom, timerRunning: self.selectedRoomTimerRunning)
            self.timePicker.countDownDuration = self.selectedRoom.gameTimeLimit
            self.timePickerView.isHidden = false
        } else {
            self.showRoomNotConnected()
        }
    }
    
    func clearClueCompletion() {
        self.ref.child("Clue Lists").child(self.selectedRoom.name).observeSingleEvent(of: .value, with: { (snapshot) in
            
            for child in snapshot.children {
                guard let snap = child as? DataSnapshot else {
                    return
                }
                
                self.ref.child("Clue Lists").child(self.selectedRoom.name).child(snap.key).child("completed").setValue("no")
            }
            
        })
    }
    
    @IBAction func resetRoomButtonPressed(_ sender: UIButton) {
        
        if selectedRoom.isConnected {
            let alert = UIAlertController(title: "Reset Room \(selectedRoom.name)", message: "This will clear all timer and chat messages from the room.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Reset", style: .default, handler: { (alert) in
                
                self.clearClueCompletion()
                self.setTimerTime(game: self.selectedRoom, timerRunning: false)
                
                self.ref.child("Current Games").child(self.selectedRoom.name).removeValue()
                self.ref.child("Current Games").child(self.selectedRoom.name).child("connectedAccessoryCount").setValue(self.selectedRoom.connectedAccessories)
                self.ref.child("Current Games").child(self.selectedRoom.name).child("connectedUserCount").setValue(self.selectedRoom.connectedRooms)
                self.ref.child("Current Games").child(self.selectedRoom.name).child("helpStatus").setValue("Connected")
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Reset Room \(selectedRoom.name)", message: "This will clear all timer and chat messages from the room.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Reset", style: .default, handler: { (alert) in
                self.clearClueCompletion()
                self.ref.child("Current Games").child(self.selectedRoom.name).removeValue()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showRoomNotConnected() {
        let alert = UIAlertController(title: "Room not connected", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return gameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: GameMasterRoomTableViewCell.reuseID) as? GameMasterRoomTableViewCell  else {
            return UITableViewCell()
        }
        
        cell.selectionStyle = .none
        gameList[indexPath.row].tableViewCell = cell
        
        cell.roomNameLabel.text = gameList[indexPath.row].name
        cell.cellHeaderBackground.applyDropShadow(radius: 0.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let visibleCell = cell as? GameMasterRoomTableViewCell else {
            return
        }
        
        let game = self.gameList[indexPath.row]
        updateHelpUI(game: game)
        
        if visibleCell.roomNameLabel.text != selectedRoom.name {
            visibleCell.timerBackgroundView.layer.borderColor = UIColor.clear.cgColor
            visibleCell.timerBackgroundView.layer.borderWidth = 0.0
        } else {
            visibleCell.timerBackgroundView.layer.borderColor = UIColor.kiwiWhiteTransparent().cgColor
            visibleCell.timerBackgroundView.layer.borderWidth = 10.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.ref.child("Clue Lists").child(self.selectedRoom.name).removeAllObservers()
        
        selectedRoom = gameList[indexPath.row]
        selectedRoomNameLabel.text = selectedRoom.name
        
        NotificationCenter.default.post(name: NSNotification.Name.init("selectedRoomChanged"), object: nil)
        
        for bob in tableView.visibleCells {
            if let cell = bob as? GameMasterRoomTableViewCell {
                cell.timerBackgroundView.layer.borderColor = UIColor.clear.cgColor
                cell.timerBackgroundView.layer.borderWidth = 0.0
            }
        }
        
        guard let cell = tableView.cellForRow(at: indexPath) as? GameMasterRoomTableViewCell else {
            return
        }
        
        cell.timerBackgroundView.layer.borderColor = UIColor.kiwiWhiteTransparent().cgColor
        cell.timerBackgroundView.layer.borderWidth = 8.0
        
        setupTimerRefForSelectedRoom()
    }
    
    func playHelpSound(game: GameRoom) {
        switch game.currentHelpStatus {
        case "Help":
            AudioServicesPlayAlertSound(helpSound)
        case "Maintenance":
            AudioServicesPlayAlertSound(helpSound)
        case "ConnectedGameOver":
            AudioServicesPlayAlertSound(1332)
        default:
            break
        }
    }
    
    func updateHelpUI(game: GameRoom) {
        
        switch game.currentHelpStatus {
        case "Help":
            game.isConnected = true
            game.tableViewCell?.roomTimerLabel.text = "Clue Request"
            game.tableViewCell?.timerBackgroundView.backgroundColor = UIColor.kiwiYellow()
        case "Maintenance":
            game.isConnected = true
            game.tableViewCell?.roomTimerLabel.text = "Maintenance"
            game.tableViewCell?.timerBackgroundView.backgroundColor = UIColor.kiwiRed()
        case "Connected":
            game.isConnected = true
            game.tableViewCell?.roomTimerLabel.text = "Connected"
            game.tableViewCell?.timerBackgroundView.backgroundColor = UIColor.kiwiDarkerGreen()
            
        case "ConnectedTimerRunning":
            game.isConnected = true
            game.tableViewCell?.roomTimerLabel.text = "Timer Running"
            game.tableViewCell?.timerBackgroundView.backgroundColor = UIColor.kiwiDarkerGreen()
        case "ConnectedTimerPaused":
            game.isConnected = true
            game.tableViewCell?.roomTimerLabel.text = "Timer Stopped"
            game.tableViewCell?.timerBackgroundView.backgroundColor = UIColor.kiwiDarkerGreen()
            
        case "ConnectedGameOverWINNER":
            game.isConnected = true
            game.gameOver = true
            game.tableViewCell?.roomTimerLabel.text = "WINNER"
            game.tableViewCell?.timerBackgroundView.backgroundColor = UIColor.kiwiMandarin()
            
            // IF ROOM IS NOT SELECTED ROOM
            if selectedRoom.name != game.name {
                self.setTimerTime(game: game, timerRunning: false)
                return
            }
            
            if selectedRoomTimerRunning {
                self.selectedRoomTimerRunning = false
                self.setTimerTime(game: selectedRoom, timerRunning: self.selectedRoomTimerRunning)
                
                sendWin(shouldSendWinStatus: false)
            }
            
        case "ConnectedGameOver":
            game.isConnected = true
            game.gameOver = true
            game.tableViewCell?.roomTimerLabel.text = "Game Over"
            game.tableViewCell?.timerBackgroundView.backgroundColor = UIColor.kiwiMandarin()
        case "Disconnected":
            game.isConnected = false
            game.tableViewCell?.roomTimerLabel.text = "Disconnected"
            game.tableViewCell?.timerBackgroundView.backgroundColor = UIColor.kiwiLightBlueGray()
        default:
            game.isConnected = false
            game.tableViewCell?.roomTimerLabel.text = "Disconnected"
            game.tableViewCell?.timerBackgroundView.backgroundColor = UIColor.kiwiLightBlueGray()
        }
    }
    
    // MARK: - Firebase Listeners
    func setUpGameRoomListListener() {
        ref.child("Games").observe(.value, with: { (snapshot) in
            //if the reference have some values
            if snapshot.childrenCount > 0 {
                
                // clearing the list
                self.gameList.removeAll()
                
                // iterating through all the values
                for child in snapshot.children {
                    if let value = (child as? DataSnapshot)?.value as? [String : Any] {
                        if let name = value["name"] as? String {
                            let game = GameRoom(name: name)
                            
                            if let timeLimit = value["timeLimit"] as? NSNumber {
                                game.gameTimeLimit = TimeInterval(truncating: timeLimit)
                            } else {
                                game.gameTimeLimit = 3600
                            }
                            
                            if let songName = value["songName"] as? String {
                                game.songName = songName
                            }
                            
                            self.gameList.append(game)
 
                            self.setUpHelpListenerForRoom(name:name)
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    //reloading the tableview
                    if self.gameList.count > 1 {
                        if self.selectedRoom.name == "" {
                            self.selectedRoom = self.gameList[0]
                        }
                    }
                    self.setupTimerRefForSelectedRoom()
                    self.roomSelectionTableView.reloadData()
                }
            }
        })
    }
    
    func setUpHelpListenerForRoom(name: String) {
        
        ref.child("Current Games").child(name).child("connectedUserCount").removeAllObservers()
        ref.child("Current Games").child(name).child("connectedUserCount").observe(.value) { (snapshot) in
            
            var count = 0
            
            if let roomCount = snapshot.value as? NSNumber {
                count = Int(truncating: roomCount)
            }
            
            if let gameIndex = self.gameList.index(where: { (daGame) -> Bool in
                return daGame.name == name
            }) {
                let game = self.gameList[gameIndex]
                game.connectedRooms = count
                DispatchQueue.main.async {
                    switch count {
                    case 0:
                        game.tableViewCell?.roomsConnectedLabel.text = "No Rooms Connected"
                        return
                    case 1:
                        game.tableViewCell?.roomsConnectedLabel.text = "1 Room Connected"
                        break
                    default:
                        game.tableViewCell?.roomsConnectedLabel.text = "\(count) Rooms Connected"
                        break
                        
                    }
                }
            }
        }
        
        ref.child("Current Games").child(name).child("connectedAccessoryCount").removeAllObservers()
        ref.child("Current Games").child(name).child("connectedAccessoryCount").observe(.value) { (snapshot) in
            
            var count = 0
            
            if let roomCount = snapshot.value as? NSNumber {
                count = Int(truncating: roomCount)
            }
            
            if let gameIndex = self.gameList.index(where: { (daGame) -> Bool in
                return daGame.name == name
            }) {
                let game = self.gameList[gameIndex]
                game.connectedAccessories = count
                
                DispatchQueue.main.async {
                    switch count {
                    case 0:
                        game.tableViewCell?.accessoryIndicatorLabel.text = "No Accessories Connected"
                        break
                    case 1:
                        game.tableViewCell?.accessoryIndicatorLabel.text = "1 Accessory Connected"
                        break
                    default:
                        game.tableViewCell?.accessoryIndicatorLabel.text = "\(count) Accessories Connected"
                        break
                        
                    }
                }
            }
        }
        
//        ref.child("Current Games").child(name).child("AccessoryConnected").removeAllObservers()
//        ref.child("Current Games").child(name).child("AccessoryConnected").observe(.value) { (snapshot) in
//
//            var connect = false
//
//            if let connected = snapshot.value as? String {
//                if connected == "true" {
//                    connect = true
//                }
//            }
//
////            if let gameIndex = self.gameList.index(where: { (daGame) -> Bool in
////                return daGame.name == name
////            }) {
////                let game = self.gameList[gameIndex]
////
////                if connect {
////                }
////            }
//        }
 
        ref.child("Current Games").child(name).child("helpStatus").removeAllObservers()
        ref.child("Current Games").child(name).child("helpStatus").observe(.value) { (snapshot) in
            
            if let helpStatus = snapshot.value as? String {
                if let gameIndex = self.gameList.index(where: { (daGame) -> Bool in
                    return daGame.name == name
                }) {
                    let game = self.gameList[gameIndex]
                    
                    game.currentHelpStatus = helpStatus
                    
                    // Figure out why this doesn't hold reference properly
                    if self.selectedRoom.name == game.name {
                        self.selectedRoom = game
                    }
                    
                    DispatchQueue.main.async {
                        self.updateHelpUI(game: game)
                        self.playHelpSound(game: game)
                    }
                }
            }
        }
    }
    
    @IBAction func masterLockSwitched(_ sender: UISwitch) {
        let value = sender.isOn
        
        roomSelectionTableView.isUserInteractionEnabled = !value
        masterLockOverlayView.isHidden = !value
    }
    
    @IBAction func timePickerChanged(_ sender: UIDatePicker) {
        // Do something maybe?
    }
    @IBAction func doneButtonPressedTimePicker(_ sender: UIButton) {
        
        // Hide picker
        timePickerView.isHidden = true
        
        // Update timer to match picker time
        if let mzTimer = self.mzTimerLabel {
            if mzTimer.counting {
                mzTimer.pause()
            }

            totalTimeTimerLabel.setStopWatchTime(selectedRoom.gameTimeLimit - timePicker.countDownDuration)
            mzTimer.setCountDownTime(timePicker.countDownDuration)
        } else {
            self.createTimer(withTime: timePicker.countDownDuration, forLabel: self.selectedRoomTimerLabel)
        }
        
        setTimerTime(game: selectedRoom, timerRunning: self.selectedRoomTimerRunning)
    }
    
    @IBAction func cancelButtonPressedTimePicker(_ sender: UIButton) {
        timePickerView.isHidden = true
    }
    
    // TODO: Hook this shizz up to some UI and make sure functionality works.
    func getChatLogForSelectedRoom() {
        ref.child("Current Games").child(selectedRoom.name).child("roomMessages").removeAllObservers()
        ref.child("Current Games").child(selectedRoom.name).child("roomMessages").observe(.value) { (snapshot) in
            
            self.chatMessagesForRoom = []
            
            for child in snapshot.children {
                
                guard let snap = child as? DataSnapshot else {
                    return
                }
                
                if let message = snap.value as? String {
                    self.chatMessagesForRoom.append(message)
                }
            }
            
            DispatchQueue.main.async {
                self.chatMessagesForRoom = self.chatMessagesForRoom.reversed()
            }
        }
    }
    
    func setupTimerRefForSelectedRoom() {
        
        self.ref.child("Current Games").child(selectedRoom.name).child("clueCount").removeAllObservers()
        self.ref.child("Current Games").child(selectedRoom.name).child("clueCount").observe(.value) { (snapshot) in
            
            var count = 0
            
            if let clueCount = snapshot.value as? NSNumber {
                count = clueCount.intValue
            }
            
            self.selectedRoom.cluesUsed = count
            
            DispatchQueue.main.async {
                self.cluesUsedLabel.text = "\(count)"
            }
        }
        
        ref.child("Current Games").child(selectedRoom.name).child("Timer Info").removeAllObservers()
        ref.child("Current Games").child(selectedRoom.name).child("Timer Info").observe(.value) { (snapshot) in
            
            guard let snap = snapshot.value as? [String : Any] else {
                
                DispatchQueue.main.async {
                    if let mzTimer = self.mzTimerLabel {
                        
                        if self.selectedRoomTimerRunning {
                            self.totalTimeTimerLabel.pause()
                            mzTimer.pause()
                        }
                        
                        self.totalTimeTimerLabel.reset()
                        self.totalTimeTimerLabel.setStopWatchTime(self.selectedRoom.gameTimeLimit - self.selectedRoom.gameTimeLimit)
                        mzTimer.setCountDownTime(self.selectedRoom.gameTimeLimit)
                    } else {
                        self.createTimer(withTime: self.selectedRoom.gameTimeLimit, forLabel: self.selectedRoomTimerLabel)
                    }
                    
                    self.selectedRoomTimerRunning = false
                    self.setTimerButton()
                }
                return
            }
            
            if let running = snap["timerRunning"] as? NSNumber {
                DispatchQueue.main.async {
                    
                    guard let timeStamp = snap["timeStamp"] as? TimeInterval else { return }
                    guard let totalRunTime = snap["totalRunTime"] as? TimeInterval else { return }
                    
                    var timeToStart = totalRunTime - (Date().timeIntervalSince1970 - timeStamp)
                    
                    // TODO: REFACTOR THIS SHIZZZ
                    if running == 0 {
                        timeToStart = totalRunTime
                    }
                    
                    if let mzTimer = self.mzTimerLabel {
                        self.totalTimeTimerLabel.removeFromSuperview()
                        mzTimer.removeFromSuperview()
                    }

                    self.selectedRoomTimerLabel.text = ""
                    self.selectedRoomTotalTimeLabel.text = ""
                    self.createTimer(withTime: timeToStart, forLabel: self.selectedRoomTimerLabel)
                    
                    if running == 0 {
                        if let mzTimer = self.mzTimerLabel {
                            self.totalTimeTimerLabel.pause()
                            mzTimer.pause()
                            self.selectedRoomTimerRunning = false
                        }
                    } else {
                        self.selectedRoom.gameOver = false
                         self.totalTimeTimerLabel.start()
                        
                        self.mzTimerLabel.start(ending: { (countTime) in
                            // Timer Finished
                            self.selectedRoomTimerRunning = false
                            self.totalTimeTimerLabel.pause()
                            self.setTimerButton()
                        })
                        
                        self.selectedRoomTimerRunning = true
                    }
                    
                    self.setTimerButton()
                }
            }
        }
    }
}
