//
//  ClueTableViewCell.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 3/16/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit

class ClueTableViewCell: UITableViewCell {

    static let reuseID = "clueCellReuse"
    
    @IBOutlet weak var clueLabel: UILabel!
    @IBOutlet weak var showHintButton: UIButton!
    @IBOutlet weak var statusSwitch: UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
