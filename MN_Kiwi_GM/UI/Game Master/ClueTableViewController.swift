//
//  ClueTableViewController.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 3/16/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import Firebase

class ClueTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var clueTableView: UITableView!
    var gameMasterVC: GameMasterController!
    var clueRef: DatabaseReference!
    var selectedClueToEdit: [String : String]? = nil
    
    var cluesListForRoom: [[String : String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleUI()
        
        clueRef = gameMasterVC.ref.child("Clue Lists").child(gameMasterVC.selectedRoom.name)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init("selectedRoomChanged"), object: nil, queue: OperationQueue.main) { (notification) in
            
            self.clueRef = self.gameMasterVC.ref.child("Clue Lists").child(self.gameMasterVC.selectedRoom.name)
            self.clueTableView.isEditing = false
            self.editButton.setTitle("Edit", for: .normal)
            
            self.listenForClues()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.clueRef = self.gameMasterVC.ref.child("Clue Lists").child(self.gameMasterVC.selectedRoom.name)
        listenForClues()
    }
    
    func listenForClues() {
        clueRef.removeAllObservers()
        
        clueRef.observe(.value) { (snapshot) in
            
            self.cluesListForRoom = []
            
            for child in snapshot.children {
                
                var clueObject: [String : String] = [:]
                
                guard let snap = child as? DataSnapshot else {
                    return
                }
                
                clueObject["key"] = snap.key
                
                if snap.hasChild("section") {
                    clueObject["section"] = snap.childSnapshot(forPath: "section").value as? String
                }
                
                if snap.hasChild("hint") {
                    clueObject["hint"] = snap.childSnapshot(forPath: "hint").value as? String
                }
                
                if snap.hasChild("step") {
                    clueObject["step"] = snap.childSnapshot(forPath: "step").value as? String
                }
                
                if snap.hasChild("completed") {
                    clueObject["completed"] = snap.childSnapshot(forPath: "completed").value as? String
                }
                
                self.cluesListForRoom.append(clueObject)
            }
            
            DispatchQueue.main.async {
                self.clueTableView.reloadData()
            }
        }
    }
    
    func styleUI() {
        self.view.layoutIfNeeded()
        clueTableView.tableFooterView = UIView()
        clueTableView.alwaysBounceVertical = false
        clueTableView.estimatedRowHeight = 80
        clueTableView.rowHeight = UITableViewAutomaticDimension
        clueTableView.allowsSelection = false
    }
    
    @IBAction func addClueButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toAddClueForm", sender: self)
    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        clueTableView.isEditing = !clueTableView.isEditing
        
        if clueTableView.isEditing {
            editButton.setTitle("Done", for: .normal)
            clueTableView.allowsSelectionDuringEditing = true
        } else {
            editButton.setTitle("Edit", for: .normal)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cluesListForRoom.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ClueTableViewCell.reuseID) as? ClueTableViewCell else {
            return UITableViewCell()
        }
        
        let clue = (cluesListForRoom[indexPath.row])
        
        if clue["completed"] == "yes"{
            cell.statusSwitch.isOn = true
        } else {
            cell.statusSwitch.isOn = false
        }
        
        cell.clueLabel.text = clue["step"]
        cell.showHintButton.tag = indexPath.row
        cell.statusSwitch.tag = indexPath.row
        
        cell.statusSwitch.addTarget(self, action: #selector(ClueTableViewController.toggleStatusSwitch(_:)), for: .valueChanged)
        cell.showHintButton.addTarget(self, action: #selector(ClueTableViewController.showHintButtonPressed(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // open add form controller and pass values for clue/hint
        selectedClueToEdit = cluesListForRoom[indexPath.row]
        self.performSegue(withIdentifier: "toAddClueForm", sender: self)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            if let key = self.cluesListForRoom[indexPath.row]["key"] {
                self.clueRef.child(key).removeValue()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.cluesListForRoom[sourceIndexPath.row]
        self.cluesListForRoom.remove(at: sourceIndexPath.row)
        self.cluesListForRoom.insert(movedObject, at: destinationIndexPath.row)
        NSLog("%@", "\(sourceIndexPath.row) => \(destinationIndexPath.row)")
        
        var count = 0
        
        for _ in self.cluesListForRoom {
            
            self.cluesListForRoom[count]["key"] = "\(count)"
            count += 1
        }
        
        print(self.cluesListForRoom)
        self.clueRef.setValue(self.cluesListForRoom)
    }
    
    @objc func toggleStatusSwitch(_ sender : UISwitch) {
        let tag = sender.tag
        
        let clue = cluesListForRoom[tag]
        guard let clueKey = clue["key"] else { return }
        
        if sender.isOn {
            self.clueRef.child(clueKey).child("completed").setValue("yes")
        } else {
            self.clueRef.child(clueKey).child("completed").setValue("no")
        }
    }
    
    @objc func showHintButtonPressed(_ sender : UIButton) {
        let tag = sender.tag
        
        guard let hintMessage = (cluesListForRoom[tag])["hint"] else {
            return
        }
        gameMasterVC.showClueText(message: hintMessage)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let addClueVC = segue.destination as? AddClueViewController {
            
            if let selectedClue = selectedClueToEdit {
                addClueVC.clueToEdit = selectedClue
                
                clueTableView.isEditing = false
                editButton.setTitle("Edit", for: .normal)
                selectedClueToEdit = nil
                clueTableView.allowsSelectionDuringEditing = false
            }
            
            addClueVC.gameMasterVC = self.gameMasterVC
        }
    }
}

