//
//  AppDelegate.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 2/20/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit
import CoreData
import Firebase

var skippedLogin = false

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()

        if Auth.auth().currentUser != nil {
            // Go straight to game selection
            skippedLogin = true
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            if let mainVC = storyboard.instantiateInitialViewController() {
                self.window?.rootViewController = mainVC
                self.window?.makeKeyAndVisible()
            }
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.

        if let roomVC = UIApplication.topViewController() as? RoomController {

            roomVC.player.stop()

            roomVC.ref.child("connectedUserCount").observeSingleEvent(of: .value) { (snapshot) in

                var count = 0

                if let roomCount = snapshot.value as? NSNumber {
                    count = roomCount.intValue - 1
                }

                if count <= 0 {
                    roomVC.ref.child("helpStatus").setValue("Disconnected")

                    // This should be uncommented if we decide to remove game object on last game iPad being removed.
//                    if (!roomVC.timerRunning)
//                    {
//                        roomVC.ref.removeAllObservers()
//                        roomVC.ref.removeValue()
//                        return
//                    }
                }

                roomVC.ref.child("connectedUserCount").setValue(max(0, count))
            }
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if let roomVC = UIApplication.topViewController() as? RoomController {
            roomVC.ref.child("connectedUserCount").observeSingleEvent(of: .value) { (snapshot) in

                var count = 1

                if let roomCount = snapshot.value as? NSNumber {
                    count = roomCount.intValue + count
                }

                roomVC.ref.child("connectedUserCount").setValue(count)
                roomVC.ref.child("helpStatus").setValue(roomVC.currentHelpStatus)
                if roomVC.timerRunning {
                    roomVC.player.play()
                }
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
}
